﻿using UnityEngine;
using System.Collections.Generic;
using Lidgren.Network;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Network manager that can function as a host, server, or client
/// </summary>
public class LidgrenNetManager : MonoBehaviour
{
    NetClient MyClient;
    NetServer MyServer;

    public NetClient myClient
    {
        get
        {
            return MyClient;
        }
    }
    public NetServer myServer
    {
        get
        {
            return MyServer;
        }
    }

    [Tooltip("Online Scene, usually the same scene as the manager")]
    public LevelIndex Online;
    [Tooltip("Offline scene, typically a main menu or something")]
    public LevelIndex Offline;

    /// <summary>
    /// If host, returns a list of valid connections besides the host. If server, returns a list of all valid connections.
    /// </summary>
    public List<NetConnection> Clients
    {
        get
        {
            if (isHost)
            {
                List<NetConnection> connections = MyServer.Connections;

                for (int i = connections.Count - 1; i > -1; i--)
                {
                    if (connections[i].RemoteUniqueIdentifier == MyClient.UniqueIdentifier ||
                        !ConnectionExists(connections[i]))
                        connections.RemoveAt(i);
                }
                return connections;

            }
            else
            {
                List<NetConnection> connections = MyServer.Connections;

                for (int i = connections.Count - 1; i > -1; i--)
                {
                    if (!ConnectionExists(connections[i]))
                        connections.RemoveAt(i);
                }
                return connections;
            }
        }
    }
    /// <summary>
    /// Returns a list of all valid clients. Includes the host clients.
    /// </summary>
    public List<NetConnection> AllClients
    {
        get
        {
            List<NetConnection> connections = MyServer.Connections;

            for (int i = connections.Count - 1; i > -1; i--)
            {
                if (!ConnectionExists(connections[i]))
                    connections.RemoveAt(i);
            }
            return connections;
        }
    }

    /// <summary>
    /// Returns all currently accounted for Network objects on the client,server, or host. Does not return any player objects.
    /// </summary>
    public IEnumerable<GameObject> AllNetworkObjects
    {
        get
        {
            foreach (GameObject g in NetObjects.Values)
                if ((g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour).PrefabID != PlayerPrefabID)
                    yield return g;
        }
    }

    /// <summary>
    /// Only returns true for clients. Returns false if not initialized
    /// </summary>
    public bool isClient
    {
        get
        {
            return MyClient != null && MyServer == null;
        }
    }
    /// <summary>
    /// Only returns true if this is just a server. Returns false if not initialized
    /// </summary>
    public bool isServer
    {
        get
        {
            return MyServer != null && MyClient == null;
        }
    }
    /// <summary>
    /// Returns true if serving as a host. Returns flase if only a server or only a client. Returns flase if not initialized yet.
    /// </summary>
    public bool isHost
    {
        get
        {
            return MyClient != null && MyServer != null;
        }
    }
    /// <summary>
    /// Returns true if this network peer is initialized, wether it be server, host, or client
    /// </summary>
    public bool isActive
    {
        get
        {
            if (isClient && !isHost)
            {
                return true;
            }
            else if (isServer && !isHost)
            {
                return true;
            }
            else if (isHost)
            {
                return true;
            }
            else
                return false;
        }
    }

    /// <summary>
    /// Returns an unused ObjectID
    /// </summary>
    int FreeID
    {
        get
        {
            int newKey = Random.Range(0, int.MaxValue);

            if (NetObjects.Count > 2000000000)
                throw (new System.Exception("Can't have more than 2 billion objects, sorry!"));

            //get a unique key
            while (NetObjects.ContainsKey(newKey) || RetiredIDs.Contains(newKey))
                newKey = Random.Range(0, int.MaxValue);

            return newKey;
        }
    }

    //Recommended not to mess with the range of the values

    [SerializeField]
    public bool DEBUG = true;
    [SerializeField]
    int use_port = 3003;
    [SerializeField]
    string connect_to_ip = "127.0.0.1";
    [SerializeField]
    string ApplicationName = "Default";
    [SerializeField]
    int MaxConnections = 32;
    [Range(0.2f, 3.0f)]
    [Tooltip("In seconds")]
    [SerializeField]
    float TimeOut = 0.5f;
    [SerializeField]
    [Tooltip("In seconds")]
    [Range(0, 1.0f)]
    float simulated_latency;
    [SerializeField]
    [Tooltip("Precentage")]
    [Range(0, 0.5f)]
    float simulated_packet_loss;

    /// <summary>
    /// Port being used for the network manager
    /// </summary>
    public int Port
    {
        get
        {
            return use_port;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set port after network has started!"));
            use_port = value;
        }
    }
    /// <summary>
    /// IP the client will connect to
    /// </summary>
    public string ConnectToIP
    {
        get
        {
            return connect_to_ip;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set ip after network has started!"));
            connect_to_ip = value;
        }
    }
    /// <summary>
    /// Name of the connection, used by LidgrenNetwork for connection verification
    /// </summary>
    public string AppName
    {
        get
        {
            return ApplicationName;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set Application Name after network has started!"));
            ApplicationName = value;
        }
    }
    /// <summary>
    /// Maximum allowed connections
    /// </summary>
    public int MaximumConnections
    {
        get
        {
            return MaxConnections;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set Maximum Connections after network has started!"));
            if (value < 0)
                throw (new System.Exception("Maximum Connections cannot be negative!"));
            MaxConnections = value;
        }
    }
    /// <summary>
    /// Timeout before server drops a connection. (seconds)
    /// </summary>
    public float Timeout
    {
        get
        {
            return TimeOut;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set Time Out after network has started!"));
            TimeOut = value;
        }
    }
    /// <summary>
    /// The simulated latency of the network
    /// </summary>
    public float SimulatedLatency
    {
        get
        {
            return simulated_latency;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set latency after network has started!"));
            simulated_latency = value;
        }
    }
    /// <summary>
    /// simulated packet loss over the network
    /// </summary>
    public float SimulatedPacketLoss
    {
        get
        {
            return simulated_packet_loss;
        }
        set
        {
            if (isActive)
                throw (new System.Exception("Cannot set packet loss after network has started!"));
            simulated_packet_loss = value;
        }
    }

    public GameObject PlayerObject;

    [Tooltip("List of objects that the server can spawn. Cannot be modified at runtime! " +
        "Please only drag prefabs here!!! The prefab should only exist in your file system, it should not be present in scene view.")]
    [SerializeField]
    List<GameObject> SpawnablePrefabs = new List<GameObject>();
    Dictionary<GameObject, int> prefabs = new Dictionary<GameObject, int>();

    /// <summary>
    /// Iterable through all spawnable prefabs
    /// </summary>
    public IEnumerable<GameObject> AllSpawnablePrefabs
    {
        get
        {
            foreach (GameObject g in SpawnablePrefabs)
                yield return g;
        }
    }

    /// <summary>
    /// All instance network objects
    /// </summary>
    Dictionary<int, GameObject> NetObjects = new Dictionary<int, GameObject>();

    /// <summary>
    /// Dictionary used for determining if specific clients can call authoritative functions on specific objects. This
    /// data structure is only valid on the server.
    /// </summary>
    Dictionary<long, HashSet<int>> ClientOwnedObjects = new Dictionary<long, HashSet<int>>();
    /// <summary>
    /// Dictionary holding gameobject references for each player object, only valid on server
    /// </summary>
    Dictionary<long, GameObject> ClientPlayerObjects = new Dictionary<long, GameObject>();

    /// <summary>
    /// Sometimes objects are created on clients after some of their messages arrive, 
    /// this structure keeps track of messages on clients for a time.
    /// </summary>
    Dictionary<int, DelayedMessage> DelayedMessages = new Dictionary<int, DelayedMessage>();

    /// <summary>
    /// Set of retired IDs for fast membership testing
    /// </summary>
    HashSet<int> RetiredIDs = new HashSet<int>();
    /// <summary>
    /// RetiredIDs organized by time
    /// </summary>
    Queue<RetiredObjectID> RetiredIDsByTime = new Queue<RetiredObjectID>();
    /// <summary>
    /// Set containing objects whos client authority was revoked recently
    /// </summary>
    HashSet<RemovedAuthorityID> authority_modifications = new HashSet<RemovedAuthorityID>();

    /// <summary>
    /// Data structure holding methods that can be called through the message system
    /// </summary>
    Dictionary<ushort, System.Action<NetIncomingMessage>> static_functions = new Dictionary<ushort, System.Action<NetIncomingMessage>>();

    //recommend not changing internal timeout and ping values

    /// <summary>
    /// Internal timeout in seconds, best not to change.
    /// </summary>
    static float internal_timeout
    {
        get
        {
            return 5.0f;
        }
    }
    /// <summary>
    /// Internal PingRate in seconds, best not to change
    /// </summary>
    static float internal_pingrate
    {
        get
        {
            return 1.0f;
        }
    }

    private static LidgrenNetManager singleton;
    /// <summary>
    /// NetManager Singleton
    /// </summary>
    public static LidgrenNetManager Singleton
    {
        get
        {
            return singleton;
        }
    }

    /// <summary>
    /// Reference to events object
    /// </summary>
    private LidgrenNetEvents myEvents;

    void Awake()
    {
        //only one singleton allowed
        if (singleton != null)
            Destroy(gameObject);
        else
        {
            singleton = this;
            myEvents = gameObject.GetComponent(typeof(LidgrenNetEvents)) as LidgrenNetEvents;
            if (myEvents == null)
                myEvents = gameObject.AddComponent<LidgrenNetEvents>();
        }
    }

    void Start()
    {
        DontDestroyOnLoad(this);

        if (Offline.value < 0 || Online.value < 0)
            throw (new System.Exception("Must have a input a valid Online and Offline scene!"));

        if (PlayerObject == null && DEBUG)
            throw (new System.Exception("Player object is null! There must be a player object."));

        for (int i = 0; i < SpawnablePrefabs.Count; i++)
        {
            if (SpawnablePrefabs[i].GetComponent(typeof(LidgrenNetBehaviour)) == null)
                throw (new System.Exception("Network Error, All SpawnablePrefabs MUST have LidgrenNetBehaviourcomponent!"));
            if (SpawnablePrefabs[i] == PlayerObject)
                throw (new System.Exception("A prefab on the prefabs list is the same as the player prefab!"));
            prefabs.Add(SpawnablePrefabs[i], i);
        }

        if (PlayerObject != null && PlayerObject.GetComponent(typeof(LidgrenNetBehaviour)) == null)
            throw (new System.Exception("Network Error, PlayerObject must have a LidgrenNetBehaviourcomponent!"));
    }

    void Update()
    {
        if (isServer || isHost)
        {
            HandleServerMessages();
        }
        if (isClient || isHost)
        {
            HandleClientMessages();
        }
    }

    void HandleServerMessages()
    {
        NetIncomingMessage message;
        while (isActive && (message = MyServer.ReadMessage()) != null)
        {
            //load online scene if not loaded
            if (SceneManager.GetActiveScene().buildIndex != Online.value)
            {
                myEvents.OnLoadOnlineScene();
                SceneManager.LoadScene(Online.value);
            }

            switch (message.MessageType)
            {
                case NetIncomingMessageType.UnconnectedData:
                    {
                        //dont handle unconnected messages if it is singleplayer
                        if (MyServer.Configuration.MaximumConnections == 1) return;

                        int msgtype = message.ReadInt32();
                        if (msgtype == (int)ObjectID.Ping)
                        {
                            //send ping back
                            NetOutgoingMessage omsg = MyServer.CreateMessage(4);
                            omsg.Write((int)ObjectID.Ping);
                            MyServer.SendUnconnectedMessage(omsg, message.SenderEndPoint);
                        }
                        break;
                    }
                case NetIncomingMessageType.Data:
                    {
                        try
                        {
                            RunMessage(message);
                        }
                        catch (System.Exception e)
                        {
                            if (DEBUG)
                                Debug.Log(e);
                        }
                        break;
                    };
                case NetIncomingMessageType.ConnectionApproval:
                    {
                        message.SenderConnection.Approve();
                        break;
                    };
                case NetIncomingMessageType.StatusChanged:
                    {
                        // handle connection status messages
                        switch (message.SenderConnection.Status)
                        {
                            case NetConnectionStatus.Connected:
                                {
                                    if (DEBUG)
                                        Debug.Log("Established a New Connection");
                                    //do new client connection operations
                                    StartCoroutine(NewClientConnectionRoutine(message));

                                    break;
                                };
                            case NetConnectionStatus.Disconnected:
                                {
                                    if (DEBUG)
                                        Debug.Log("A Client Disconnected ");

                                    //call virtual function
                                    if (isServer || (isHost && message.SenderConnection.RemoteUniqueIdentifier != myClient.UniqueIdentifier))
                                        myEvents.OnClientDisconnect(message.SenderConnection);

                                    DestroyAllObjectAuthorities(message.SenderConnection);
                                    DestroyPlayer(message.SenderConnection);

                                    break;
                                };
                            default:
                                break;
                        }
                        break;
                    }
                case NetIncomingMessageType.DebugMessage:
                    {
                        if (DEBUG)
                            Debug.Log(message.ReadString());
                        break;
                    }
                case NetIncomingMessageType.WarningMessage:
                    {
                        if (DEBUG)
                            Debug.Log(message.ReadString());
                        break;
                    }
                default:
                    Debug.Log("unhandled message with type: "
                        + message.MessageType);
                    break;
            }
        }
    }

    void HandleClientMessages()
    {
        NetIncomingMessage message;
        while (isActive && (message = MyClient.ReadMessage()) != null)
        {
            //load online scene if not loaded
            if (SceneManager.GetActiveScene().buildIndex != Online.value)
            {
                myEvents.OnLoadOnlineScene();
                SceneManager.LoadScene(Online.value);
            }

            switch (message.MessageType)
            {
                case NetIncomingMessageType.Data:
                    {
                        try
                        {
                            // handle custom messages
                            RunMessage(message);
                        }
                        catch (System.Exception e)
                        {
                            if (DEBUG)
                                Debug.Log("An Error occured while reading a message sent from the server: " + e.ToString());
                        }
                        break;
                    }

                case NetIncomingMessageType.StatusChanged:
                    // handle connection status messages
                    switch (message.SenderConnection.Status)
                    {
                        case NetConnectionStatus.Connected:
                            {
                                if (DEBUG)
                                    Debug.Log("Connected");

                                //call virtual connect
                                if (!isHost)
                                    myEvents.OnConnect();

                                break;
                            };
                        case NetConnectionStatus.Disconnected:
                            {
                                if (DEBUG)
                                {
                                    Debug.Log("Disconnected");
                                }

                                //call virtual function
                                if (!isHost)
                                    myEvents.OnDisconnect();

                                //goto offline scene!
                                if (isClient)
                                {
                                    //reset
                                    Reset();
                                    myEvents.OnLoadOfflineScene();
                                    SceneManager.LoadScene(Offline.value);
                                }
                                //host needs to shutdown server as well
                                else
                                {
                                    //shutdown server if needed
                                    if (MyServer != null)
                                    {
                                        MyServer.Shutdown("Server Shutdown");
                                        Reset();
                                    }
                                    //load offline scene if needed
                                    if (SceneManager.GetActiveScene().buildIndex != Offline.value)
                                    {
                                        myEvents.OnLoadOfflineScene();
                                        SceneManager.LoadScene(Offline.value);
                                    }
                                }

                                break;
                            };
                        default:
                            break;
                    }
                    break;
                case NetIncomingMessageType.DebugMessage:
                    {
                        if (DEBUG)
                            Debug.Log(message.ReadString());
                        break;
                    }
                case NetIncomingMessageType.WarningMessage:
                    {
                        if (DEBUG)
                            Debug.Log(message.ReadString());
                        break;
                    }
                default:
                    Debug.Log("unhandled message with type: "
                        + message.MessageType);
                    break;
            }
        }
    }

    /// <summary>
    /// Initializes the new connection, and spawns its player and other objects
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    IEnumerator NewClientConnectionRoutine(NetIncomingMessage message)
    {
        if (isServer || (isHost && message.SenderConnection.RemoteUniqueIdentifier != myClient.UniqueIdentifier))
            yield return myEvents.OnInitializeClient(message.SenderConnection);

        //if the new client is not the host client, then spawn all objects on it
        if (isServer || (isHost && message.SenderConnection.RemoteUniqueIdentifier != myClient.UniqueIdentifier))
            SpawnObjects(message.SenderConnection);

        //spawn player for client
        SpawnPlayer(message.SenderConnection);

        //update syncvars to new client
        foreach (GameObject g in NetObjects.Values)
        {
            LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
            nb.SendSyncVarUpdate(message.SenderConnection);
        }

        //call virtual function
        if (isServer || (isHost && message.SenderConnection.RemoteUniqueIdentifier != myClient.UniqueIdentifier))
            myEvents.OnClientConnect(message.SenderConnection);
    }

    /// <summary>
    /// Resets everything. Only call this when offline
    /// </summary>
    void Reset()
    {
        NetObjects = new Dictionary<int, GameObject>();
        ClientOwnedObjects = new Dictionary<long, HashSet<int>>();
        ClientPlayerObjects = new Dictionary<long, GameObject>();
        DelayedMessages = new Dictionary<int, DelayedMessage>();
        RetiredIDs = new HashSet<int>();
        RetiredIDsByTime = new Queue<RetiredObjectID>();
        MyServer = null;
        MyClient = null;
    }

    void OnApplicationQuit()
    {
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client Disconnected!");
            MyClient.Disconnect("Disconnected!");
            Reset();
        }
        else if (isServer)
        {
            if (DEBUG)
                Debug.Log("Server Shutdown!");
            MyServer.Shutdown("Server shutting down!");
            Reset();
        }
        else if (isHost)
        {
            if (DEBUG)
            {
                Debug.Log("Host Client Disconnected!");
                Debug.Log("Host Server Shutdown!");
            }
            MyClient.Disconnect("Disconnected!");
            MyServer.Shutdown("Server shutting down!");
            Reset();
        }
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == Offline.value)
        {
            static_functions = new Dictionary<ushort, System.Action<NetIncomingMessage>>();
            NetObjects = new Dictionary<int, GameObject>();
            ClientOwnedObjects = new Dictionary<long, HashSet<int>>();
            ClientPlayerObjects = new Dictionary<long, GameObject>();
            DelayedMessages = new Dictionary<int, DelayedMessage>();
            RetiredIDs = new HashSet<int>();
            RetiredIDsByTime = new Queue<RetiredObjectID>();
            authority_modifications = new HashSet<RemovedAuthorityID>();
        }
    }

    /// <summary>
    /// Starts this NetworkManager as a server
    /// </summary>
    public void StartServer()
    {
        if (isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot start network, network is already active!");
            return;
        }

        if (string.IsNullOrEmpty(ApplicationName))
            ApplicationName = "Default";

        var config = new NetPeerConfiguration(ApplicationName)
        {
            Port = use_port,
            MaximumConnections = MaxConnections,
            ConnectionTimeout = internal_timeout,
            PingInterval = internal_pingrate,
            DefaultOutgoingMessageCapacity = 20,
            SimulatedMinimumLatency = simulated_latency,
            SimulatedRandomLatency = simulated_latency * 0.5f,
            SimulatedLoss = simulated_packet_loss
        };
        config.SetMessageTypeEnabled(NetIncomingMessageType.UnconnectedData, true);
        MyServer = new NetServer(config);
        MyServer.Start();

        if (DEBUG)
            Debug.Log("Started server at port: " + MyServer.Port.ToString());
    }
    /// <summary>
    /// Start this NetManager as a client
    /// </summary>
    public void StartClient()
    {
        if (isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot start network, network is already active!");
            return;
        }

        var config = new NetPeerConfiguration(ApplicationName)
        {
            DefaultOutgoingMessageCapacity = 20,
            ConnectionTimeout = internal_timeout,
            PingInterval = internal_pingrate,
        };
        var client = new NetClient(config);
        client.Start();
        client.Connect(host: connect_to_ip, port: use_port);
        MyClient = client;

        if (DEBUG)
            Debug.Log("Starting client connection at: " + connect_to_ip + " , port: " + use_port.ToString());

        StartCoroutine(ClientTask());
    }
    /// <summary>
    /// Start this NetManager as a host
    /// </summary>
    public void StartHost()
    {
        if (MyServer != null || MyClient != null)
        {
            if (DEBUG)
                Debug.Log("Cannot start network, network is already active!");
            return;
        }

        //start server
        if (string.IsNullOrEmpty(ApplicationName))
            ApplicationName = "Default";

        var sconfig = new NetPeerConfiguration(ApplicationName)
        {
            Port = use_port,
            MaximumConnections = MaxConnections,
            ConnectionTimeout = internal_timeout,
            PingInterval = internal_pingrate,
            DefaultOutgoingMessageCapacity = 20,
            SimulatedMinimumLatency = simulated_latency,
            SimulatedRandomLatency = simulated_latency * 0.5f,
            SimulatedLoss = simulated_packet_loss
        };
        sconfig.SetMessageTypeEnabled(NetIncomingMessageType.UnconnectedData, true);
        MyServer = new NetServer(sconfig);
        MyServer.Start();

        if (DEBUG)
            Debug.Log("Started server at port: " + MyServer.Port.ToString());
        //end start server

        //start client
        var config = new NetPeerConfiguration(ApplicationName)
        {
            ConnectionTimeout = internal_timeout,
            PingInterval = internal_pingrate,
            DefaultOutgoingMessageCapacity = 20
        };
        var client = new NetClient(config);
        client.Start();
        client.Connect(host: "127.0.0.1", port: use_port);
        MyClient = client;

        if (DEBUG)
            Debug.Log("Starting client connection at: 127.0.0.1 , port: " + use_port.ToString());
        //end start client

        StartCoroutine(ServerTask());
    }

    /// <summary>
    /// Ping the input address, calls the callback when received. This function waits for a responding ping from the server for
    /// 'LidgrenNatManager.Timeout' seconds, if none is received, then the ping time is set to float.max. 
    /// Only servers/hosts are responsive to ping requests. It is recommended to ping multiple times (like 3-4 times) for the best results.
    /// </summary>
    /// <param name="ip"> IP to ping </param>
    /// <param name="port"> Port to use, uses the default Lidgren Netmanager port if not specified</param>
    /// <param name="callback"> Function that accepts a float (ping latency time in seconds) </param>
    public void PingThis(string ip, System.Action<float> callback, int port = -1)
    {
        if (callback == null)
        {
            throw (new System.Exception("Input call back function for ping routine is null!"));
        }

        if (ip == null)
        {
            throw (new System.Exception("Input IP for ping routine is null!"));
        }

        if (port == -1)
            port = use_port;
        StartCoroutine(PingRoutine(ip, callback, port));
    }
    IEnumerator PingRoutine(string ip, System.Action<float> callback, int port)
    {
        //start peer
        var config = new NetPeerConfiguration(ApplicationName);
        config.SetMessageTypeEnabled(NetIncomingMessageType.UnconnectedData, true);
        NetPeer p = new NetPeer(config);
        p.Start();

        //send messages
        NetOutgoingMessage msg = p.CreateMessage();
        msg.Write((int)ObjectID.Ping);
        p.SendUnconnectedMessage(msg, ip, port);
        yield return 0;
        yield return 0;
        yield return 0;

        //record time
        float timeAtStart = Time.realtimeSinceStartup;
        float latency = float.MaxValue;

        bool connected = false;

        //wait for timeout
        while (Time.realtimeSinceStartup - timeAtStart < TimeOut)
        {
            //attempt to read msg
            NetIncomingMessage imsg = null;
            if ((imsg = p.ReadMessage()) != null)
            {
                if (imsg.MessageType == NetIncomingMessageType.UnconnectedData)
                {
                    //if the message is a ping
                    int msgtype = imsg.ReadInt32();
                    if (msgtype == (int)ObjectID.Ping)
                    {
                        connected = true;
                        if (DEBUG)
                            Debug.Log("Server responded to ping in " + (Time.realtimeSinceStartup - timeAtStart).ToString() + " seconds");
                        break;
                    }
                }
            }
            yield return 0;
        }

        //record latency
        if (connected)
            latency = Time.realtimeSinceStartup - timeAtStart;

        if (callback == null)
        {
            throw (new System.Exception("Input call back function for ping routine is null!"));
        }

        //call function
        callback(latency);
    }

    /// <summary>
    /// Ping the input address, calls the callback when received. This function waits for a responding ping from the server for
    /// 'LidgrenNatManager.Timeout' seconds, if none is received, then the ping time is set to float.max. 
    /// Only servers/hosts are responsive to ping requests. It is recommended to ping multiple times (like 3-4 times) for the best results.
    /// Overloaded version callback also sets a string (ip) and int (port) parameter in the callback
    /// </summary>
    /// <param name="ip"> IP to ping </param>
    /// <param name="port"> Port to use, uses the default Lidgren Netmanager port if not specified</param>
    /// <param name="callback"> Function that accepts a float (ping latency time in seconds) </param>
    public void PingThis(string ip, System.Action<float, string, int> callback, int port = -1)
    {
        if (callback == null)
        {
            throw (new System.Exception("Input call back function for ping routine is null!"));
        }

        if (ip == null)
        {
            throw (new System.Exception("Input IP for ping routine is null!"));
        }

        if (port == -1)
            port = use_port;
        StartCoroutine(PingRoutine(ip, callback, port));
    }
    IEnumerator PingRoutine(string ip, System.Action<float, string, int> callback, int port)
    {
        //start peer
        var config = new NetPeerConfiguration(ApplicationName);
        config.SetMessageTypeEnabled(NetIncomingMessageType.UnconnectedData, true);
        NetPeer p = new NetPeer(config);
        p.Start();

        //send messages
        NetOutgoingMessage msg = p.CreateMessage();
        msg.Write((int)ObjectID.Ping);
        p.SendUnconnectedMessage(msg, ip, port);
        yield return 0;
        yield return 0;
        yield return 0;

        //record time
        float timeAtStart = Time.realtimeSinceStartup;
        float latency = float.MaxValue;

        bool connected = false;

        //wait for timeout
        while (Time.realtimeSinceStartup - timeAtStart < TimeOut)
        {
            //attempt to read msg
            NetIncomingMessage imsg = null;
            if ((imsg = p.ReadMessage()) != null)
            {
                if (imsg.MessageType == NetIncomingMessageType.UnconnectedData)
                {
                    //if the message is a ping
                    int msgtype = imsg.ReadInt32();
                    if (msgtype == (int)ObjectID.Ping)
                    {
                        connected = true;
                        if (DEBUG)
                            Debug.Log("Server responded to ping in " + (Time.realtimeSinceStartup - timeAtStart).ToString() + " seconds");
                        break;
                    }
                }
            }
            yield return 0;
        }

        //record latency
        if (connected)
            latency = Time.realtimeSinceStartup - timeAtStart;

        if (callback == null)
        {
            throw (new System.Exception("Input call back function for ping routine is null!"));
        }

        //call function
        callback(latency, ip, port);
    }

    /// <summary>
    /// Look at client information and perform actions, like kicking clients with slow connection.
    /// </summary>
    /// <returns></returns>
    IEnumerator ServerTask()
    {
        while (isActive)
        {
            yield return 0;
            if (isServer || isHost)
            {
                //handle low latency connections
                foreach (NetConnection c in MyServer.Connections)
                {
                    if (c.AverageRoundtripTime > TimeOut)
                        Kick(c, "Kicking for slow ping response: " + ((int)(c.AverageRoundtripTime * 1000)).ToString() +
                            "ms. Maximum allowed ping is " + ((int)(TimeOut * 1000)).ToString() + "ms.");
                }

                //handle retired IDs, dequeue them if they have waited long enough
                if (RetiredIDsByTime.Count > 0)
                    while (TryUnRetireNext()) ;

                //remove old authority modifications
                Stack<RemovedAuthorityID> remove = null;
                foreach (RemovedAuthorityID id in authority_modifications)
                    if (id.TimeSinceRetired > internal_timeout * 2 + TimeOut + 5.0f)
                    {
                        if (remove == null) remove = new Stack<RemovedAuthorityID>();
                        remove.Push(id);
                    }
                if (remove != null)
                    while (remove.Count > 0)
                        authority_modifications.Remove(remove.Pop());
            }
        }
    }

    /// <summary>
    /// Slowly frees retired IDs on the client
    /// </summary>
    /// <returns></returns>
    IEnumerator ClientTask()
    {
        while (isActive)
        {
            yield return 0;
            if (isClient)
            {
                //handle retired IDs, dequeue them if they have waited long enough
                if (RetiredIDsByTime.Count > 0)
                    while (TryUnRetireNext(internal_timeout * 1.5f + TimeOut)) ;
                //end handle retired IDs

                //handle delayed messages that have been around too long
                Stack<int> remove_delayed_msgs = null;
                Stack<int> run_delayed_msgs = null;
                if (DelayedMessages.Count > 0)
                    foreach (DelayedMessage msg in DelayedMessages.Values)
                    {
                        //if delayed for a long time, then remove
                        if (msg.TimeDelayed > internal_timeout * 1.5f + TimeOut)
                        {
                            if (remove_delayed_msgs == null) remove_delayed_msgs = new Stack<int>();
                            remove_delayed_msgs.Push(msg.ID);
                        }
                        else if (NetObjects.ContainsKey(msg.ID))
                        {
                            if (run_delayed_msgs == null) run_delayed_msgs = new Stack<int>();
                            run_delayed_msgs.Push(msg.ID);
                        }
                    }
                //remove
                if (remove_delayed_msgs != null)
                {
                    if (remove_delayed_msgs.Count > 0 && DEBUG)
                        Debug.Log("Queued messages were waiting for an object that never arrived, removing....");
                    while (remove_delayed_msgs.Count > 0)
                        DelayedMessages.Remove(remove_delayed_msgs.Pop());
                }
                //run
                if (run_delayed_msgs != null)
                {
                    while (run_delayed_msgs.Count > 0)
                        RunDelayedMessages(run_delayed_msgs.Pop());
                }
                //end handle delayed messages
            }
        }
    }

    /// <summary>
    /// Run an incoming message based on its type
    /// </summary>
    /// <param name="msg"></param>
    void RunMessage(NetIncomingMessage msg)
    {
        int msgtype;
        if (msg.ReadInt32(out msgtype))
        {
            //switch statement to handle message types
            switch (msgtype)
            {
                case (int)ObjectID.StaticMessage:
                    {
                        ushort key = msg.ReadUInt16();
                        if (static_functions.ContainsKey(key))
                        {
                            try
                            {
                                //run function
                                static_functions[key](msg);
                            }
                            catch (System.Exception e)
                            {
                                if (DEBUG)
                                    Debug.Log("Error running function, on key " + key.ToString() + ", error: " + e.ToString());
                            }
                        }
                        else if (DEBUG)
                            Debug.Log("Error, they key " + key.ToString() + " was not found in the static functions dictionary!");

                        break;
                    }
                case (int)ObjectID.SpawnGameObject:
                    {
                        if (isServer || isHost)
                        {
                            if (DEBUG)
                                Debug.Log("Internal Error, spawn object is only run on clients!");
                            return;
                        }

                        int msg_value;
                        if (msg.ReadInt32(out msg_value))
                        {
                            int p_id = msg.ReadInt32();
                            bool isMine = msg.ReadBoolean();

                            //error if exists
                            if (NetObjects.ContainsKey(p_id))
                            {
                                if (DEBUG)
                                    Debug.Log("Error spawning player, id already exists!");
                                return;
                            }

                            GameObject newObj = Instantiate(SpawnablePrefabs[msg_value]);
                            LidgrenNetBehaviour nb = newObj.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                            nb.ObjectNetID = p_id;
                            nb.HaveAuthority = isMine;
                            nb.PrefabID = msg_value;
                            nb.transform.position = msg.ReadVector3();
                            nb.transform.localScale = msg.ReadVector3();
                            nb.transform.rotation = msg.ReadQuaternion();

                            NetObjects.Add(nb.ObjectNetID, newObj);
                        }
                        else if (DEBUG)
                            Debug.Log("Error occured while getting object to spawn!");
                        break;
                    }
                case (int)ObjectID.SpawnObject:
                    {
                        if (isServer || isHost)
                        {
                            if (DEBUG)
                                Debug.Log("Internal Error, spawn object is only run on clients!");
                            return;
                        }

                        int msg_value;
                        if (msg.ReadInt32(out msg_value))
                        {
                            int p_id = msg.ReadInt32();
                            bool isMine = msg.ReadBoolean();

                            //error if exists
                            if (NetObjects.ContainsKey(p_id))
                            {
                                if (DEBUG)
                                    Debug.Log("Error spawning player, id already exists!");
                                return;
                            }

                            GameObject newObj = Instantiate(SpawnablePrefabs[msg_value]);
                            LidgrenNetBehaviour nb = newObj.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                            nb.ObjectNetID = p_id;
                            nb.HaveAuthority = isMine;
                            nb.PrefabID = msg_value;
                            NetObjects.Add(nb.ObjectNetID, newObj);
                        }
                        else if (DEBUG)
                            Debug.Log("Error occured while getting object to spawn!");
                        break;
                    }
                case (int)ObjectID.SpawnPlayer:
                    {
                        if (isServer || isHost)
                        {
                            if (DEBUG)
                                Debug.Log("Internal Error, spawn player is only run on clients!");
                            return;
                        }

                        //read fields
                        int p_id = msg.ReadInt32();
                        bool isMine = msg.ReadBoolean();

                        //error if exists
                        if (NetObjects.ContainsKey(p_id))
                        {
                            if (DEBUG)
                                Debug.Log("Error spawning player, id already exists!");
                            return;
                        }

                        if (PlayerObject != null)
                        {
                            //make player
                            GameObject newPlayer = Instantiate(PlayerObject);
                            LidgrenNetBehaviour nb = newPlayer.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                            nb.ObjectNetID = p_id;
                            nb.HaveAuthority = isMine;
                            nb.PrefabID = PlayerPrefabID;
                            NetObjects.Add(nb.ObjectNetID, newPlayer);
                        }
                        else if (DEBUG)
                            Debug.Log("Player object is null, cannot spawn player object!");
                        break;
                    }
                case (int)ObjectID.DestroyObject:
                    {
                        if (isServer || isHost)
                        {
                            if (DEBUG)
                                Debug.Log("Internal Error, spawn player is only run on clients!");
                            return;
                        }

                        int msg_value;
                        if (msg.ReadInt32(out msg_value))
                        {
                            GameObject g = NetObjects[msg_value];
                            NetObjects.Remove(msg_value);
                            Retire(msg_value);
                            Destroy(g);
                        }
                        else if (DEBUG)
                            Debug.Log("Error occured while trying to destroy object!");
                        break;
                    }
                case (int)ObjectID.SpawnAllObjects:
                    {
                        if (isServer || isHost)
                        {
                            if (DEBUG)
                                Debug.Log("Internal Error, spawn player is only run on clients!");
                            return;
                        }

                        int count = msg.ReadInt32();

                        for (int i = 0; i < count; i++)
                        {
                            int pfb_id = msg.ReadInt32();
                            int obj_id = msg.ReadInt32();
                            Vector3 pos = msg.ReadVector3();
                            Quaternion rot = msg.ReadQuaternion();
                            Vector3 scale = msg.ReadVector3();
                            GameObject newObj = pfb_id == -1 ? Instantiate(PlayerObject) : Instantiate(SpawnablePrefabs[pfb_id]);
                            LidgrenNetBehaviour nb = newObj.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                            nb.ObjectNetID = obj_id;
                            nb.HaveAuthority = false;
                            nb.PrefabID = pfb_id;
                            newObj.transform.position = pos;
                            newObj.transform.rotation = rot;
                            newObj.transform.localScale = scale;
                            NetObjects.Add(nb.ObjectNetID, newObj);
                        }
                        break;
                    }
                case (int)ObjectID.None:
                    {
                        if (DEBUG)
                            Debug.Log("Send Unitialized Object, Error!");
                        break;
                    }
                case (int)ObjectID.NetMessage:
                    {
                        myEvents.OnReceiveMessage(msg);
                        break;
                    }
                case (int)ObjectID.ChangeAuthority:
                    {
                        Debug.Log("Change authority message!");
                        int obj_id = msg.ReadInt32();
                        bool value = msg.ReadBoolean();
                        GameObject g = NetObjects[obj_id];
                        LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                        nb.HaveAuthority = value;
                        break;
                    }
                default:
                    {
                        //is this is syncvar update?
                        bool isSyncVarUpdate = msgtype == (int)ObjectID.SyncVarMessage;
                        //if so get the objectid
                        if (isSyncVarUpdate)
                            msgtype = msg.ReadInt32();

                        //else, run message for input object and msg type
                        if (!NetObjects.ContainsKey(msgtype))
                        {
                            //1) this could happen under functional circumstances
                            //example: send a message to an object, then send a delete message for that object
                            //if the delete message arrives first, then the object will not be able to receive other incoming messages

                            //another example
                            //2) An object is created on the server, and then sends a message to a client
                            //The object does not get created on the client before the message arrives, so the message cannot be read

                            // For 1), There are many ways to fix this, I just used an easy way, which is to retire object IDs and not allow them to be
                            // used after being deleted for a time period

                            // For 2), We use queues on the client that hold received messages for a period of time (waiting for the object to be created)

                            // retiring IDs on the server also prevents rare errors in which incorrect objects are found on the client
                            // Eg:
                            // An object is created on the server then sends a message and then is soon after destroyed.
                            // Then another object is created on the server and happens to get the same random ID
                            // The messege to the client is slow and arrives after the second object is created on the client
                            // This results in the wrong object getting the message

                            if (isClient)
                            {
                                //If ID is retired, then the object is destroyed and won't be able to get a message
                                if (RetiredIDs.Contains(msgtype))
                                {
                                    if (DEBUG)
                                        Debug.Log("Potential Error, didn't find object ID! Object is already destroyed.");
                                    return;
                                }
                                //if the object has not been destroyed, then it has not been created yet, so delay the message
                                else
                                {
                                    //queue message
                                    DelayMessage(msg, msgtype, isSyncVarUpdate);

                                    if (DEBUG)
                                        Debug.Log("Message arriving but no object exists yet, queueing message for object: " + msgtype.ToString() + "...");
                                }

                                return;
                            }
                            else if (isServer || isHost)
                            {
                                //If ID is retired, then the object is destroyed and won't be able to get a message
                                if (RetiredIDs.Contains(msgtype))
                                {
                                    if (DEBUG)
                                        Debug.Log("Potential Error, didn't find object ID! Object is already destroyed.");
                                    return;
                                }
                                else if (DEBUG)
                                    Debug.Log("Error, received a message but don't have an object to call it on.");

                                //the server doesn't queue messages because messages can't arrive before objects are created
                                return;
                            }
                        }
                        else
                        {
                            GameObject obj = NetObjects[msgtype];
                            LidgrenNetBehaviour nb = obj.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                            byte functiontype = isSyncVarUpdate ? (byte)0 : msg.ReadByte();
                            //check if allowed, if not throw exception
                            //only done on host/client
                            if (!isSyncVarUpdate && !isClient && nb.FunctionIsAuthoritative(functiontype))
                            {
                                if (ClientOwnedObjects.ContainsKey(msg.SenderConnection.RemoteUniqueIdentifier))
                                {
                                    if (authority_modifications.Contains(new RemovedAuthorityID(0.0f, msgtype)))
                                    {
                                        //this can occur due to latency, the client can send a message that reaches the server after authority was revoked
                                        //but before the clients local authority was updated
                                        if (DEBUG)
                                            Debug.Log("Possible error, Received authoritative message from the client, but authority was recently revoked.");
                                        return;
                                    }
                                    else
                                    {
                                        //if there is no reference to the input connection in permissions...
                                        if (ClientOwnedObjects[msg.SenderConnection.RemoteUniqueIdentifier] == null)
                                        {
                                            if (DEBUG)
                                                Debug.Log("Tried to call authoritative function on the server from a client without permission!");
                                            return;
                                        }
                                        //if there is no reference to the input object in permissions...
                                        if (!ClientOwnedObjects[msg.SenderConnection.RemoteUniqueIdentifier].Contains(msgtype))
                                        {
                                            if (DEBUG)
                                                Debug.Log("Tried to call authoritative function on the server from a client without permission!");
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    if (DEBUG)
                                        Debug.Log("Tried to call authoritative function on the server from a client without permission!");
                                    return;
                                }
                            }

                            //if there are any delayed messages, then handle them!
                            if (isClient)
                            {
                                RunDelayedMessages(msgtype);
                            }

                            //run incoming msg
                            if (!isSyncVarUpdate)
                                nb.RunMessage(functiontype, msg);
                            else
                                nb.RunSyncVarMessage(msg);
                        }
                        break;
                    }
            }
        }
        else
        {
            if (DEBUG)
                Debug.Log("Internal Error occured while reading message, failed to get message type!");
        }
    }

    /// <summary>
    /// Runs delayed messages
    /// </summary>
    /// <param name="msgtype"></param>
    /// <param name="isSyncVarUpdate"></param>
    /// <param name="nb"></param>
    void RunDelayedMessages(int msgtype)
    {
        LidgrenNetBehaviour nb = NetObjects[msgtype].GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
        if (DelayedMessages.ContainsKey(msgtype))
        {
            while (DelayedMessages[msgtype].MsgCount > 0)
            {
                NetIncomingMessage delayed_msg = DelayedMessages[msgtype].DequeueMsg();
                bool isSyncVarUpdate = DelayedMessages[msgtype].DequeueSyncVar();
                if (!isSyncVarUpdate)
                {
                    byte functiontype = delayed_msg.ReadByte();
                    if (nb.FunctionIsAuthoritative(functiontype))
                        nb.RunMessage(functiontype, delayed_msg);
                    else if (DEBUG)
                        Debug.Log("Tried to run a function from the client " + functiontype.ToString() + " but, the client does not have authority");
                }
                else
                    nb.RunSyncVarMessage(delayed_msg);
            }
            DelayedMessages.Remove(msgtype);
            if (DEBUG)
                Debug.Log("Ran delayed messages on client for object: " + msgtype.ToString() + "!");
        }
    }

    /// <summary>
    /// Meant to be called from the Host or the Server. This also instantiates GameObjects on hosts/servers. This function is
    /// meant to only accept GameObjects that are prefabs
    /// </summary>
    /// <param name="g"></param>
    public GameObject SpawnPrefab(GameObject g)
    {
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return null;
        }

        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn object, server is inactive");
            return null;
        }

        if (g == null || !prefabs.ContainsKey(g))
        {
            if (DEBUG)
                Debug.Log("Couldn't find prefab for an input object. 'SpawnGameObject' can only spawn prefabs that exist on the SpawnablePrefabs list!");
            return null;
        }

        //make new object on server 
        GameObject newObj = Instantiate(g);
        LidgrenNetBehaviour nb = newObj.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
        nb.ObjectNetID = FreeID;
        nb.HaveAuthority = true;
        nb.PrefabID = prefabs[g];
        NetObjects.Add(nb.ObjectNetID, newObj);

        List<NetConnection> clients = Clients;
        //if there is others besides host...
        if (clients.Count > 0)
        {
            //Write to clients
            NetOutgoingMessage outmsg = MyServer.CreateMessage(13);
            outmsg.Write((int)ObjectID.SpawnObject);
            outmsg.Write(prefabs[g]);
            outmsg.Write(nb.ObjectNetID);
            outmsg.Write(false);

            MyServer.SendMessage(outmsg, clients, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
        }

        return newObj;
    }

    /// <summary>
    /// Spawns the prefab with authority for the input connections. The connections are assumed to belong to clients (or the host client)
    /// </summary>
    /// <param name="g"></param>
    /// <param name="GiveAuthorityTo"></param>
    public GameObject SpawnPrefabWithAuthority(GameObject g, NetConnection GiveAuthorityTo)
    {
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return null;
        }

        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn object, server is inactive");
            return null;
        }

        if (g == null || !prefabs.ContainsKey(g))
        {
            if (DEBUG)
                Debug.Log("Couldn't find prefab for an input object. 'SpawnGameObject' can only spawn prefabs that exist on the SpawnablePrefabs list!");
            return null;
        }

        if (GiveAuthorityTo == null)
        {
            if (DEBUG)
                Debug.Log("Input connections authority list is empty!");
            return null;

        }

        //determine if host gets authority
        bool HostGetsAuthoirty = MyClient == null ? false : GiveAuthorityTo.RemoteUniqueIdentifier == MyClient.UniqueIdentifier;

        //make new object on server 
        GameObject newObj = Instantiate(g);
        LidgrenNetBehaviour nb = newObj.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
        nb.ObjectNetID = FreeID;
        nb.HaveAuthority = HostGetsAuthoirty;
        nb.PrefabID = prefabs[g];
        NetObjects.Add(nb.ObjectNetID, newObj);
        //add authority if appropriate
        if (HostGetsAuthoirty)
            AddAuthority(MyClient.UniqueIdentifier, nb);

        List<NetConnection> clients = Clients;
        //if there is others besides host...
        if (clients.Count > 0)
        {
            foreach (NetConnection c in clients)
            {
                //add authority if appropriate
                if (c.RemoteUniqueIdentifier == GiveAuthorityTo.RemoteUniqueIdentifier)
                    AddAuthority(c.RemoteUniqueIdentifier, nb);
                //Write to clients
                NetOutgoingMessage outmsg = MyServer.CreateMessage(13);
                outmsg.Write((int)ObjectID.SpawnObject);
                outmsg.Write(prefabs[g]);
                outmsg.Write(nb.ObjectNetID);
                outmsg.Write(c.RemoteUniqueIdentifier == GiveAuthorityTo.RemoteUniqueIdentifier);
                //send
                MyServer.SendMessage(outmsg, c, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
            }
        }

        return newObj;
    }

    /// <summary>
    /// This function spawns an already instantiated game object onto the network. 
    /// The GameObect must have a corresponding prefab on the prefabs list and must of a LidgrenNetBehaviour component.
    /// </summary>
    /// <param name="g"></param>
    /// <returns></returns>
    public GameObject SpawnGameObject(GameObject g)
    {
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return null;
        }

        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn object, server is inactive");
            return null;
        }

        //make new object on server 
        LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;

        if (nb == null)
            throw (new System.Exception("Error, can only spawn game objects with LidgrenNetBehaviour components!"));

        //get prefab
        GameObject newObj = SpawnablePrefabs[nb.PrefabID];

        nb.ObjectNetID = FreeID;
        nb.HaveAuthority = true;
        nb.PrefabID = prefabs[newObj];
        NetObjects.Add(nb.ObjectNetID, g);

        List<NetConnection> clients = Clients;
        //if there is others besides host...
        if (clients.Count > 0)
        {
            //Write to clients
            NetOutgoingMessage outmsg = MyServer.CreateMessage(13);
            outmsg.Write((int)ObjectID.SpawnGameObject);
            outmsg.Write(prefabs[newObj]);
            outmsg.Write(nb.ObjectNetID);
            outmsg.Write(false);
            outmsg.Write(g.transform.position);
            outmsg.Write(g.transform.localScale);
            outmsg.Write(g.transform.rotation);

            MyServer.SendMessage(outmsg, clients, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
        }

        return newObj;
    }

    /// <summary>
    /// This function spawns an already instantiated game object onto the network. 
    /// The GameObect must have a corresponding prefab on the prefabs list and must of a LidgrenNetBehaviour component.
    /// This function will give authority to the input connection.
    /// </summary>
    /// <param name="g"></param>
    /// <returns></returns>
    public GameObject SpawnGameObjectWithAuthority(GameObject g, NetConnection GiveAuthorityTo)
    {
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return null;
        }

        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn object, server is inactive");
            return null;
        }

        if (GiveAuthorityTo == null)
        {
            if (DEBUG)
                Debug.Log("Input connections authority list is empty!");
            return null;

        }

        //determine if host gets authority
        bool HostGetsAuthoirty = MyClient == null ? false : GiveAuthorityTo.RemoteUniqueIdentifier == MyClient.UniqueIdentifier;


        //make new object on server 
        LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;

        if (nb == null)
            throw (new System.Exception("Error, can only spawn game objects with LidgrenNetBehaviour components!"));

        //get prefab
        GameObject newObj = SpawnablePrefabs[nb.PrefabID];

        nb.ObjectNetID = FreeID;
        nb.HaveAuthority = HostGetsAuthoirty;
        nb.PrefabID = prefabs[newObj];
        NetObjects.Add(nb.ObjectNetID, g);
        //add authority if appropriate
        if (HostGetsAuthoirty)
            AddAuthority(MyClient.UniqueIdentifier, nb);

        List<NetConnection> clients = Clients;
        //if there is others besides host...
        if (clients.Count > 0)
        {
            foreach (NetConnection c in clients)
            {
                //add authority if appropriate
                if (c.RemoteUniqueIdentifier == GiveAuthorityTo.RemoteUniqueIdentifier)
                    AddAuthority(c.RemoteUniqueIdentifier, nb);
                //Write to clients
                NetOutgoingMessage outmsg = MyServer.CreateMessage(13);
                outmsg.Write((int)ObjectID.SpawnGameObject);
                outmsg.Write(prefabs[newObj]);
                outmsg.Write(nb.ObjectNetID);
                outmsg.Write(c.RemoteUniqueIdentifier == GiveAuthorityTo.RemoteUniqueIdentifier);
                outmsg.Write(g.transform.position);
                outmsg.Write(g.transform.localScale);
                outmsg.Write(g.transform.rotation);

                //send
                MyServer.SendMessage(outmsg, c, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
            }
        }

        return newObj;
    }

    /// <summary>
    /// Destroys a network gameobject across all clients and server. Can only be called by server/host! Also destroys the input GameObject
    /// on the host/server. Only use this function to destroy network objects! Destroying LidgrenBehaviour objects without using this function
    /// will cause synchronization errors. Do not destroy player objects with this function, this will cause syncronization errors.
    /// Player objects are automatically removed on disconnect.
    /// </summary>
    /// <param name="g"></param>
    public void DestroyGameObject(GameObject g)
    {
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot destroy objects!");
            return;
        }
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot destroy GameObjects, server is inactive!");
            return;
        }
        if (g == null || !NetObjects.ContainsKey(((LidgrenNetBehaviour)g.GetComponent(typeof(LidgrenNetBehaviour))).ObjectNetID))
        {
            if (DEBUG)
                Debug.Log("Couldn't find GameObject to destroy on Network data structures, canceling!");
            return;
        }

        LidgrenNetBehaviour nb = (LidgrenNetBehaviour)g.GetComponent(typeof(LidgrenNetBehaviour));

        if (nb.PrefabID == PlayerPrefabID)
            throw (new System.Exception("Cannot delete player objects, they get deleted automatically on disconnect"));

        NetObjects.Remove(nb.ObjectNetID);
        RemoveObjectAuthority(nb.ObjectNetID);
        Retire(nb.ObjectNetID);

        List<NetConnection> clients = Clients;
        //tell remote clients to destroy stuff
        if (clients.Count > 0)
        {
            NetOutgoingMessage outmsg = MyServer.CreateMessage(8);
            outmsg.Write((int)ObjectID.DestroyObject);
            outmsg.Write(nb.ObjectNetID);
            MyServer.SendMessage(outmsg, clients, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
        }

        Destroy(g);
    }

    /// <summary>
    /// Spawns a player on the input connection and all clients and server
    /// </summary>
    /// <param name="conn"></param>
    void SpawnPlayer(NetConnection conn)
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn player, server is inactive."); return;
        }

        if (PlayerObject != null)
        {
            if (isClient)
            {
                if (DEBUG)
                    Debug.Log("Client cannot spawn objects!");
                return;
            }

            //if spawning to host
            if (isHost && conn.RemoteUniqueIdentifier == MyClient.UniqueIdentifier)
            {
                //initialize host player
                GameObject p = Instantiate(PlayerObject);
                LidgrenNetBehaviour nb = p.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                nb.HaveAuthority = true;
                nb.ObjectNetID = FreeID;
                nb.PrefabID = PlayerPrefabID;
                NetObjects.Add(nb.ObjectNetID, p);
                ClientPlayerObjects.Add(conn.RemoteUniqueIdentifier, p);
                AddAuthority(conn.RemoteUniqueIdentifier, nb);

                //if there is more than just the host connection...
                if (myServer.Connections.Count > 1)
                {
                    //send player to everyone but host, with ismine = false, and the newID
                    NetOutgoingMessage outmsg = MyServer.CreateMessage(12);
                    outmsg.Write((int)ObjectID.SpawnPlayer);
                    outmsg.Write(nb.ObjectNetID);
                    outmsg.Write(false);
                    MyServer.SendMessage(outmsg, Clients, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
                }
            }
            //else if spawning to client
            else
            {
                //initialize host object
                GameObject p = Instantiate(PlayerObject);
                LidgrenNetBehaviour nb = p.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                nb.HaveAuthority = false;
                nb.ObjectNetID = FreeID;
                nb.PrefabID = PlayerPrefabID;
                NetObjects.Add(nb.ObjectNetID, p);
                ClientPlayerObjects.Add(conn.RemoteUniqueIdentifier, p);
                AddAuthority(conn.RemoteUniqueIdentifier, nb);

                //get a list of connections that is all connections but 'conn' and the host client
                List<NetConnection> connections = MyServer.Connections;
                for (int i = connections.Count - 1; i > -1; i--)
                    if ((isHost && connections[i].RemoteUniqueIdentifier == MyClient.UniqueIdentifier) ||
                        connections[i].RemoteUniqueIdentifier == conn.RemoteUniqueIdentifier)
                        connections.RemoveAt(i);

                //if there is someone to send this to...
                if (connections.Count > 0)
                {
                    //send player to everyone but host and target client, with ismine = false, and the newID
                    NetOutgoingMessage outmsg = MyServer.CreateMessage(12);
                    outmsg.Write((int)ObjectID.SpawnPlayer);
                    outmsg.Write(nb.ObjectNetID);
                    outmsg.Write(false);

                    //send to everyone but host client and 'conn' client
                    MyServer.SendMessage(outmsg, connections, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
                }

                //now send just to 'conn', this tells the 'conn' connection that they own the player
                NetOutgoingMessage outmsg2 = MyServer.CreateMessage(12);
                outmsg2.Write((int)ObjectID.SpawnPlayer);
                outmsg2.Write(nb.ObjectNetID);
                outmsg2.Write(true);

                MyServer.SendMessage(outmsg2, conn, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
            }
        }
        else if (DEBUG)
            Debug.Log("Player object is null, cannot spawn player object!");
    }
    /// <summary>
    /// Destroy the player belonging to the input connection
    /// </summary>
    /// <param name="conn"></param>
    void DestroyPlayer(NetConnection conn)
    {
        try
        {
            if (isClient)
            {
                if (DEBUG)
                    Debug.Log("Client cannot destroy objects!");
                return;
            }
            if (!isActive)
            {
                if (DEBUG)
                    Debug.Log("Cannot destroy GameObjects, server is inactive!");
                return;
            }

            GameObject g = ClientPlayerObjects[conn.RemoteUniqueIdentifier];
            LidgrenNetBehaviour nb = (LidgrenNetBehaviour)g.GetComponent(typeof(LidgrenNetBehaviour));

            if (g == null || !NetObjects.ContainsKey(((LidgrenNetBehaviour)g.GetComponent(typeof(LidgrenNetBehaviour))).ObjectNetID))
            {
                if (DEBUG)
                    Debug.Log("Couldn't find GameObject to destroy on Network data structures, canceling!");
                return;
            }

            ClientPlayerObjects.Remove(conn.RemoteUniqueIdentifier);
            NetObjects.Remove(nb.ObjectNetID);
            RemoveObjectAuthority(nb.ObjectNetID);
            Retire(nb.ObjectNetID);

            List<NetConnection> clients = Clients;
            //tell remote clients to destroy stuff
            if (clients.Count > 0)
            {
                NetOutgoingMessage outmsg = MyServer.CreateMessage(8);
                outmsg.Write((int)ObjectID.DestroyObject);
                outmsg.Write(nb.ObjectNetID);
                MyServer.SendMessage(outmsg, clients, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
            }

            Destroy(g);
        }
        catch (System.Exception e)
        {
            if (DEBUG)
                Debug.Log(e.ToString());
        }
    }

    /// <summary>
    /// Spawns all objects with their updated transforms on the input connection
    /// </summary>
    /// <param name="conn"></param>
    void SpawnObjects(NetConnection conn)
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn player, server is inactive."); return;
        }
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return;
        }

        //sending to host for some reason
        if (isHost && conn.RemoteUniqueIdentifier == MyClient.UniqueIdentifier)
        {
            if (DEBUG)
                Debug.Log("Internal Error, host doesn't need to spawn all objects!"); return;
        }
        //sending to client
        else
        {
            NetOutgoingMessage outmsg = MyServer.CreateMessage(NetObjects.Count * 12 * 4 + 8);
            outmsg.Write((int)ObjectID.SpawnAllObjects);
            outmsg.Write(NetObjects.Count);
            foreach (GameObject g in NetObjects.Values)
            {
                LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                outmsg.Write(nb.PrefabID);
                outmsg.Write(nb.ObjectNetID);
                outmsg.Write(g.transform.position);
                outmsg.Write(g.transform.rotation);
                outmsg.Write(g.transform.localScale);
            }
            MyServer.SendMessage(outmsg, conn, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
        }
    }

    /// <summary>
    /// Give the input connection authority over the input behaviour
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="behaviour"></param>
    void AddAuthority(long id, LidgrenNetBehaviour behaviour)
    {
        long key = id;
        HashSet<int> set;
        if (ClientOwnedObjects.TryGetValue(key, out set))
        {
            set.Add(behaviour.ObjectNetID);
        }
        else
        {
            set = new HashSet<int>();
            set.Add(behaviour.ObjectNetID);
            ClientOwnedObjects.Add(key, set);
        }
    }

    /// <summary>
    /// Destroys all GameObjects the input connection has authroity over besides the player and clears their authorities
    /// </summary>
    /// <param name="connection"></param>
    void DestroyAllObjectAuthorities(NetConnection connection)
    {
        if (ClientOwnedObjects.ContainsKey(connection.RemoteUniqueIdentifier))
        {
            if (ClientOwnedObjects[connection.RemoteUniqueIdentifier] != null)
            {
                Stack<GameObject> destroy_objects = new Stack<GameObject>();
                //find objects to delete
                foreach (int id in ClientOwnedObjects[connection.RemoteUniqueIdentifier])
                {
                    GameObject obj = NetObjects[id];
                    LidgrenNetBehaviour nb = (LidgrenNetBehaviour)obj.GetComponent(typeof(LidgrenNetBehaviour));
                    if (nb.PrefabID == PlayerPrefabID)
                        continue;
                    if (nb.PrefabID < 0 && DEBUG)
                        Debug.Log("Found unitialized NetBehaviour object!");
                    destroy_objects.Push(obj);
                }
                //delete
                while (destroy_objects.Count > 0)
                    DestroyGameObject(destroy_objects.Pop());
            }
        }

        RemoveAllObjectAuthorities(connection);
    }
    /// <summary>
    /// Remove all object authorites for the input connection
    /// </summary>
    /// <param name="connection"></param>
    void RemoveAllObjectAuthorities(NetConnection connection)
    {
        ClientOwnedObjects.Remove(connection.RemoteUniqueIdentifier);
    }
    /// <summary>
    /// Remove an object from the authority dictionary, usually because the object is being deleted
    /// </summary>
    /// <param name="ObjectID"></param>
    void RemoveObjectAuthority(int ObjectID)
    {
        Stack<long> removal_stack = null;
        foreach (KeyValuePair<long, HashSet<int>> val in ClientOwnedObjects)
            if (val.Value != null && val.Value.Contains(ObjectID))
            {
                val.Value.Remove(ObjectID);
                if (val.Value.Count == 0)
                {
                    if (removal_stack == null)
                        removal_stack = new Stack<long>();
                    removal_stack.Push(val.Key);
                }
            }
        if (removal_stack != null)
            while (removal_stack.Count > 0)
                ClientOwnedObjects.Remove(removal_stack.Pop());
    }

    /// <summary>
    /// Can only be caled on the server, re-assigns authority on the input object to 'value'.
    /// If value == true, then the server will get authority and the input connection will lose it. 
    /// If value == false, then the input connection will get authority and the server will lose it.
    /// </summary>
    /// <param name="game_object"></param>
    /// <param name="client"></param>
    public void AssignAuthority(GameObject game_object, NetConnection client, bool value)
    {
        if (!isActive)
            throw (new System.Exception("Cannot assign authority, server is inactive."));
        if (isClient)
            throw (new System.Exception("Client cannot change authority!"));

        if (isHost && ConnectionIsHost(client))
            throw (new System.Exception("Cannot reassign authority from the host client to the host server."));

        LidgrenNetBehaviour nb = game_object.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
        if (nb == null)
            throw (new System.Exception("Input object must have a networkbehaviour!"));

        if (nb.PrefabID == PlayerPrefabID)
            throw (new System.Exception("Cannot change authority on the player object!"));

        if (!NetObjects.ContainsKey(nb.ObjectNetID))
            throw (new System.Exception("Could not find the input network object!"));

        //if removing server authority
        if (value == false)
        {
            //return if it is the same
            if (value == nb.HaveAuthority) return;

            //we are removing server authority, and granting authority on the client

            //if this is the host, then we must remove the host client authority
            if (isHost)
            {
                HashSet<int> objects = null;
                ClientOwnedObjects.TryGetValue(MyClient.UniqueIdentifier, out objects);
                if (objects != null)
                    objects.Remove(nb.ObjectNetID);
            }

            //attempt to remove this object from authority modifications
            authority_modifications.Remove(new RemovedAuthorityID(Time.realtimeSinceStartup, nb.ObjectNetID));

            //add authority to client
            AddAuthority(client.RemoteUniqueIdentifier, nb);

            //set authority to false
            nb.HaveAuthority = false;

            //send message
            NetOutgoingMessage msg = MyServer.CreateMessage(5);
            msg.Write((int)ObjectID.ChangeAuthority);
            msg.Write(nb.ObjectNetID);
            msg.Write(true);
            MyServer.SendMessage(msg, client, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
        }
        //else we are adding server authority
        else
        {
            //return if it is the same
            if (value == nb.HaveAuthority) return;

            //adding server authority, remove authority from client

            //remove authority from client
            HashSet<int> objects = null;
            ClientOwnedObjects.TryGetValue(client.RemoteUniqueIdentifier, out objects);
            if (objects != null)
                objects.Remove(nb.ObjectNetID);

            //add authority to host
            if (isHost)
                AddAuthority(MyClient.UniqueIdentifier, nb);

            //change authority
            nb.HaveAuthority = true;

            //mark the authority as revoked on the client
            authority_modifications.Add(new RemovedAuthorityID(Time.realtimeSinceStartup, nb.ObjectNetID));

            //send message to client
            NetOutgoingMessage msg = MyServer.CreateMessage(5);
            msg.Write((int)ObjectID.ChangeAuthority);
            msg.Write(nb.ObjectNetID);
            msg.Write(false);
            MyServer.SendMessage(msg, client, NetDeliveryMethod.ReliableOrdered, (int)Channels.ReliableOrdered);
        }
    }

    /// <summary>
    /// Returns a list of game objects that the input client has authority over, excluding its player.
    /// The NetConnection parameter is not required if this function is invoked on the client.
    /// </summary>
    /// <param name="connection"></param>
    /// <returns></returns>
    public List<GameObject> GetClientObjects(NetConnection connection = null)
    {
        if (!isActive)
            throw (new System.Exception("Cannot GetCLientObjects, network is inactive"));

        if (isHost && connection.RemoteUniqueIdentifier == MyClient.UniqueIdentifier)
            throw (new System.Exception("Cannot get client objects for the host. The host has authority over all server authority objects."));

        List<GameObject> objects = new List<GameObject>();

        if (!isClient)
        {
            HashSet<int> player_objects = null;
            ClientOwnedObjects.TryGetValue(connection.RemoteUniqueIdentifier, out player_objects);
            if (player_objects != null)
            {
                foreach (int val in player_objects)
                {
                    objects.Add(NetObjects[val]);
                }
            }
        }
        else
        {
            foreach (GameObject g in NetObjects.Values)
            {
                LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                if (nb == null)
                    throw (new System.Exception("Error, a network object is missing its NetBehaviour!"));
                if (nb.HaveAuthority)
                    objects.Add(g);
            }
        }
        return objects;
    }

    /// <summary>
    /// Returns the player object for the input client connection.
    /// The NetConnection parameter is not required if this function is invoked on the client.
    /// </summary>
    /// <param name="connection"></param>
    /// <returns></returns>
    public GameObject GetClientPlayerObject(NetConnection connection = null)
    {
        if (!isActive)
            throw (new System.Exception("Cannot GetCLientObjects, network is inactive"));

        if (isHost && connection.RemoteUniqueIdentifier == MyClient.UniqueIdentifier)
            throw (new System.Exception("Cannot get client objects for the host. The host has authority over all server authority objects."));

        GameObject obj = null;
        if (!isClient)
        {
            ClientPlayerObjects.TryGetValue(connection.RemoteUniqueIdentifier, out obj);
        }
        else
        {
            foreach (GameObject g in NetObjects.Values)
            {
                LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
                if (nb == null)
                    throw (new System.Exception("Error, a network object is missing its NetBehaviour!"));
                if (nb.PrefabID == PlayerPrefabID)
                {
                    obj = nb.gameObject;
                    break;
                }
            }
        }
        return obj;
    }

    /// <summary>
    /// Retire an Object ID
    /// </summary>
    /// <param name="id"></param>
    void Retire(int id)
    {
        RetiredIDs.Add(id);
        RetiredObjectID obj_id = new RetiredObjectID(Time.realtimeSinceStartup, id);
        RetiredIDsByTime.Enqueue(obj_id);
    }
    /// <summary>
    /// Trys to unretire the next element in the retired queue, returns true if succeeded
    /// </summary>
    /// <returns></returns>
    bool TryUnRetireNext(float duration = 0.0f)
    {
        if (RetiredIDsByTime.Count <= 0) return false;

        float retired_duration = duration == 0.0f ? internal_timeout * 2 + 20.0f + TimeOut * 2 : duration;
        RetiredObjectID obj_id = RetiredIDsByTime.Peek();
        if (obj_id.TimeSinceRetired > retired_duration)
        {
            RetiredIDs.Remove(obj_id.ID);
            RetiredIDsByTime.Dequeue();
            if (isClient)
            {
                if (DEBUG)
                    Debug.Log("Un Retired ID on Client: " + obj_id.ID.ToString());
            }
            else
            {
                if (DEBUG)
                    Debug.Log("Un Retired ID on Server: " + obj_id.ID.ToString());
            }
            return true;
        }
        return false;
    }

    /// <summary>
    /// Delay an message (client only)
    /// </summary>
    /// <param name="msg"></param>
    /// <param name="obj_id"></param>
    void DelayMessage(NetIncomingMessage msg, int obj_id, bool sync_varmsg)
    {
        if (!isActive)
        {
            Debug.Log("Cannot delay, server is inactive.");
            return;
        }
        if (isHost || isServer)
        {
            if (DEBUG)
                Debug.Log("Messages can only be delayed on a remote client!");
            return;
        }

        if (DelayedMessages.ContainsKey(obj_id))
        {
            DelayedMessages[obj_id].Enqueue(msg, sync_varmsg);
        }
        else
        {
            Queue<NetIncomingMessage> m_queue = new Queue<NetIncomingMessage>();
            m_queue.Enqueue(msg);
            Queue<bool> s_queue = new Queue<bool>();
            s_queue.Enqueue(sync_varmsg);
            DelayedMessages.Add(obj_id, new DelayedMessage(m_queue, Time.realtimeSinceStartup, obj_id, s_queue));
        }
    }

    /// <summary>
    /// Kicks the input NetConnection from the server
    /// </summary>
    /// <param name="conn"></param>
    public void Kick(NetConnection conn, string reason = "Kicked from Server")
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn player, server is inactive."); return;
        }
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return;
        }

        if (DEBUG)
            Debug.Log(reason);

        conn.Deny(reason);
    }

    /// <summary>
    /// Shutsdown if server, disconnects if client.
    /// </summary>
    public void LeaveNetwork()
    {
        if (isServer)
        {
            MyServer.Shutdown("Server Shutdown");
            Reset();
            if (DEBUG)
                Debug.Log("Shutdown server...");
            myEvents.OnLoadOfflineScene();
            SceneManager.LoadScene(Offline.value);
        }
        else if (isHost)
        {
            MyClient.Disconnect("Host left");
            MyServer.Shutdown("Server Shutdown");
            Reset();
            if (DEBUG)
            {
                Debug.Log("Shutdown server...");
                Debug.Log("Shutdown Client...");
            }
            myEvents.OnLoadOfflineScene();
            SceneManager.LoadScene(Offline.value);
        }
        else if (isClient)
        {
            MyClient.Disconnect("Left Game");
            Reset();
            if (DEBUG)
                Debug.Log("Shutdown Client...");
            myEvents.OnLoadOfflineScene();
            SceneManager.LoadScene(Offline.value);
        }
    }
    /// <summary>
    /// Determines if the server has an input connection
    /// </summary>
    /// <param name="conn"></param>
    /// <returns></returns>
    public bool ConnectionExists(NetConnection conn)
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Cannot spawn player, server is inactive."); return false;
        }
        if (isClient)
        {
            if (DEBUG)
                Debug.Log("Client cannot spawn objects!");
            return false;
        }

        return ClientPlayerObjects.ContainsKey(conn.RemoteUniqueIdentifier);
    }
    /// <summary>
    /// Determines if a net object exists on the server or client or host
    /// </summary>
    /// <param name="net_object"></param>
    /// <returns></returns>
    public bool ObjectExists(GameObject net_object)
    {
        if (!isActive)
            throw (new System.Exception("Error, network is inactive"));
        LidgrenNetBehaviour nb = net_object.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;
        if (nb == null)
            throw (new System.Exception("Input object must have a networkbehaviour!"));
        return NetObjects.ContainsKey(nb.ObjectNetID);
    }
    /// <summary>
    /// Determines if a net object exists on the server or client or host
    /// </summary>
    /// <param name="net_object"></param>
    /// <returns></returns>
    public bool ObjectExists(LidgrenNetBehaviour net_object)
    {
        if (!isActive)
            throw (new System.Exception("Error, network is inactive"));

        return NetObjects.ContainsKey(net_object.ObjectNetID);
    }
    /// <summary>
    /// Returns true if the input connection is the host
    /// </summary>
    /// <param name="connection"></param>
    /// <returns></returns>
    public bool ConnectionIsHost(NetConnection connection)
    {
        if (!isActive)
            throw (new System.Exception("Error, network is inactive"));
        if (isClient || isServer)
            return false;
        return connection.RemoteUniqueIdentifier == MyClient.UniqueIdentifier;
    }

    /// <summary>
    /// Returns true if the input connection has authority over the input object
    /// </summary>
    /// <param name="c"></param>
    /// <param name="net_object"></param>
    /// <returns></returns>
    public bool HasAuthority(NetConnection c, LidgrenNetBehaviour net_object)
    {
        if (!isActive)
            throw (new System.Exception("Error, inactive netowrk"));
        if (isClient)
            throw (new System.Exception("Error, HasAuthority(connection,object) can only be called on the server or host"));
        if (!ClientOwnedObjects.ContainsKey(c.RemoteUniqueIdentifier)) return false;
        if (authority_modifications.Contains(new RemovedAuthorityID(0.0f, net_object.ObjectNetID)))
        {
            //this can occur due to latency, the client can send a message that reaches the server after authority was revoked
            //but before the clients local authority was updated
            if (DEBUG)
                Debug.Log("Possible error, Received authoritative message from the client, but authority was recently revoked.");
            return false;
        }
        if (ClientOwnedObjects[c.RemoteUniqueIdentifier] == null) return false;
        return ClientOwnedObjects[c.RemoteUniqueIdentifier].Contains(net_object.ObjectNetID);
    }

    /// <summary>
    /// Get a new outgoung message that will be sent to remote network managers
    /// </summary>
    public NetOutgoingMessage GetNewMessage()
    {
        NetOutgoingMessage msg = MyServer.CreateMessage();
        msg.Write((int)ObjectID.NetMessage);
        return msg;
    }
    /// <summary>
    /// Sends a message. Sends a message to all clients but the host client if running as the server or host. Sends a message
    /// to the server if running as a client.
    /// </summary>
    /// <param name="msg"></param>
    public void Send(NetOutgoingMessage msg, NetDeliveryMethod deliveryMethod, int channel)
    {
        if (isClient)
        {
            MyClient.SendMessage(msg, deliveryMethod, channel);
        }
        else if (isHost)
        {
            List<NetConnection> clients = Clients;
            if (clients.Count > 0)
                MyServer.SendMessage(msg, clients, deliveryMethod, channel);
        }
        else if (isServer)
        {
            MyServer.SendMessage(msg, Clients, deliveryMethod, channel);
        }
        else
        {
            throw (new System.Exception("Cannot send a message if the network is not active!"));
        }
    }
    /// <summary>
    /// Only can be sent by the server or host, sends a message.
    /// Does nothing if the message is sent to the host client
    /// </summary>
    /// <param name="msg"></param>
    public void Send(NetConnection connection, NetOutgoingMessage msg, NetDeliveryMethod deliveryMethod, int channel)
    {
        if (isClient)
        {
            throw (new System.Exception("Sending a message to a specific connection can only be done by the server!"));
        }
        else if (isHost)
        {
            if (connection.RemoteUniqueIdentifier != MyClient.UniqueIdentifier)
                MyServer.SendMessage(msg, connection, deliveryMethod, channel);
        }
        else if (isServer)
        {
            MyServer.SendMessage(msg, connection, deliveryMethod, channel);
        }
        else
        {
            throw (new System.Exception("Cannot send a message if the network is not active!"));
        }
    }

    /// <summary>
    /// Returns a new message with a 'StaticMessage' type.
    /// Static messages work like the LidgrenNetBehaviour messages but they are not bound to specific objects.
    /// Functions are not automatically cleared from the underlying data structure. Use LidgrenNetManager.RegisterStaticMessage to register
    /// static messages
    /// </summary>
    /// <returns></returns>
    public NetOutgoingMessage GetNewStaticMessage(ushort id)
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Request new message on inactive network!");
            return null;
        }
        if (isServer || isHost)
        {
            NetOutgoingMessage msg = MyServer.CreateMessage();
            msg.Write((int)ObjectID.StaticMessage);
            msg.Write(id);
            return msg;
        }
        else
        {
            NetOutgoingMessage msg = MyClient.CreateMessage();
            msg.Write((int)ObjectID.StaticMessage);
            msg.Write(id);
            return msg;
        }
    }
    /// <summary>
    /// Register a static message function
    /// </summary>
    /// <param name="key"></param>
    /// <param name="function"></param>
    public void RegisterStaticMessage(ushort key, System.Action<NetIncomingMessage> function)
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Request register message on inactive network!");
            return;
        }

        static_functions.Add(key, function);
    }
    /// <summary>
    /// un register a static message. Returns result of the operation.
    /// </summary>
    /// <param name="key"></param>
    public bool UnRegisterStaticMessage(ushort key)
    {
        if (!isActive)
        {
            if (DEBUG)
                Debug.Log("Request un register message on inactive network!");
            return false;
        }

        return static_functions.Remove(key);
    }

    /// <summary>
    /// Returns the prefab at the input index
    /// </summary>
    /// <param name="prefab_id"></param>
    /// <returns></returns>
    public GameObject GetPrefab(int prefab_id)
    {
        if (prefab_id < 0 || prefab_id >= SpawnablePrefabs.Count)
            throw (new System.Exception("Prefab with id: " + prefab_id.ToString() + " does not exist."));
        return SpawnablePrefabs[prefab_id];
    }
    /// <summary>
    /// Get a game object given its ObjectNetiD
    /// </summary>
    /// <param name="net_object_id"></param>
    /// <returns></returns>
    public GameObject GetObject(int net_object_id)
    {
        if (!NetObjects.ContainsKey(net_object_id))
            throw (new System.Exception("Could not find a network object with id of " + net_object_id));
        return NetObjects[net_object_id];
    }

    public enum ObjectID : int
    {
        /// <summary>
        /// Syncvar message for lidgren netbehaviour components
        /// </summary>
        SyncVarMessage = -11,
        /// <summary>
        /// Static messages that aren't tied to an object
        /// </summary>
        StaticMessage = -10,
        /// <summary>
        /// Spawn an already instantiated game object
        /// </summary>
        SpawnGameObject = -9,
        /// <summary>
        /// message type to signal a change in object authority.
        /// </summary>
        ChangeAuthority = -8,
        /// <summary>
        /// Message type to signal that a message is being sent to the NetManager
        /// </summary>
        NetMessage = -7,
        /// <summary>
        /// Ping message type
        /// </summary>
        Ping = -6,
        /// <summary>
        /// None
        /// </summary>
        None = -5,
        /// <summary>
        /// Message type that signals objects to be spawned
        /// </summary>
        SpawnAllObjects = -4,
        /// <summary>
        /// Message type to signal destroying a net object
        /// </summary>
        DestroyObject = -3,
        /// <summary>
        /// Message type to signal spawning an prefab
        /// </summary>
        SpawnObject = -2,
        /// <summary>
        /// Message type to signal spawning a player
        /// </summary>
        SpawnPlayer = -1
    }

    /// <summary>
    /// Channels used for sending data (Similar to DeliveryMethod), there can be a maximum of 32 channels in use (0 to 31)
    /// You can specify data to be sent on different channels. For example, sending data that uses ReliableOrdered delivery
    /// on a different channel than UnreliableUnordered makes it so the unreliable channel doesn't have to wait for the reliable
    /// ordered channel (because they are on different channels)
    /// </summary>
    public enum Channels
    {
        /// <summary>
        /// Channel that syncvar updates are sent over
        /// </summary>
        SyncVarChannel = 4,
        /// <summary>
        /// Default channel for reliable sequenced messages
        /// </summary>
        ReliableSequenced = 2,
        /// <summary>
        /// Default channel for reliable unordered messages
        /// </summary>
        ReliableUnordered = 0,
        /// <summary>
        /// Channel used for reliable ordered messages. GabeObject spawning synchronization use this channel
        /// </summary>
        ReliableOrdered = 1,
        /// <summary>
        /// Default channel that LidgrenNetTransform components send their messages over
        /// </summary>
        TransformUpdate = 31
    }

    /// <summary>
    /// PrefabID of player objects
    /// </summary>
    public static int PlayerPrefabID
    {
        get
        {
            return -1;
        }
    }
    /// <summary>
    /// Default PrefabID
    /// </summary>
    public static int UnitializedPrefabID
    {
        get
        {
            return int.MinValue;
        }
    }

    /// <summary>
    /// structure for holding delayed messages
    /// </summary>
    struct DelayedMessage
    {
        Queue<NetIncomingMessage> msg_queue;
        Queue<bool> syncvartype;
        float RealTimeFirstArrived;

        /// <summary>
        /// Time message has been delayed since first request
        /// </summary>
        public float TimeDelayed
        {
            get
            {
                return Time.realtimeSinceStartup - RealTimeFirstArrived;
            }
        }

        int id;
        public int ID
        {
            get
            {
                return id;
            }
        }

        public DelayedMessage(Queue<NetIncomingMessage> queue, float time_of_first, int _id, Queue<bool> sync_queue)
        {
            msg_queue = queue;
            RealTimeFirstArrived = time_of_first;
            id = _id;
            syncvartype = sync_queue;
        }

        public void Enqueue(NetIncomingMessage msg, bool syncVar)
        {
            if (msg_queue == null)
                msg_queue = new Queue<NetIncomingMessage>();
            msg_queue.Enqueue(msg);

            if (syncvartype == null)
                syncvartype = new Queue<bool>();
            syncvartype.Enqueue(syncVar);
        }

        public int MsgCount
        {
            get
            {
                return msg_queue == null ? 0 : msg_queue.Count;
            }
        }

        public NetIncomingMessage DequeueMsg()
        {
            return msg_queue.Dequeue();
        }
        public bool DequeueSyncVar()
        {
            return syncvartype.Dequeue();
        }
    }
    /// <summary>
    /// ObjectIDs get retired for ~30seconds after being destroyed to prevent some synchronization issues
    /// </summary>
    struct RetiredObjectID
    {
        float RealTimeOfRetire;
        /// <summary>
        /// Total time this ID has been retired
        /// </summary>
        public float TimeSinceRetired
        {
            get
            {
                return Time.realtimeSinceStartup - RealTimeOfRetire;
            }
        }

        int id;
        /// <summary>
        /// The ID that is retired
        /// </summary>
        public int ID
        {
            get
            {
                return id;
            }
        }

        public RetiredObjectID(float time, int _id)
        {
            id = _id; RealTimeOfRetire = time;
        }
    }
    /// <summary>
    /// Id representing an object that recently had its client authority revoked
    /// </summary>
    struct RemovedAuthorityID
    {
        float RealTimeOfRetire;
        /// <summary>
        /// Total time this ID has been retired
        /// </summary>
        public float TimeSinceRetired
        {
            get
            {
                return Time.realtimeSinceStartup - RealTimeOfRetire;
            }
        }

        int id;
        /// <summary>
        /// The ID that is retired
        /// </summary>
        public int ID
        {
            get
            {
                return id;
            }
        }

        public RemovedAuthorityID(float time, int _id)
        {
            id = _id; RealTimeOfRetire = time;
        }

        public override bool Equals(object obj)
        {
            return obj is RemovedAuthorityID ? ((RemovedAuthorityID)obj).id == id : false;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
