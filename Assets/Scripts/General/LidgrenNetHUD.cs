﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LidgrenNetManager))]
public class LidgrenNetHUD : MonoBehaviour
{

    void OnGUI()
    {
        if (LidgrenNetManager.Singleton == null) return;

        if (!LidgrenNetManager.Singleton.isActive)
        {
            if (GUI.Button(new Rect(10, 10, 120, 30), "Server Only"))
                LidgrenNetManager.Singleton.StartServer();

            if (GUI.Button(new Rect(10, 40, 120, 30), "Client Only"))
                LidgrenNetManager.Singleton.StartClient();

            if (GUI.Button(new Rect(10, 70, 120, 30), "Host"))
                LidgrenNetManager.Singleton.StartHost();
        }
        else
        {
            if (GUI.Button(new Rect(10, 10, 120, 30), "Disconnect"))
                LidgrenNetManager.Singleton.LeaveNetwork();
        }
    }

}
