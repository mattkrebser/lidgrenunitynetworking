﻿using UnityEngine;
using System.Collections;
using Lidgren.Network;

public class LidgrenNetTransform : LidgrenNetBehaviour
{
    //NetTransforms sync from the server to all clients
    //they also sync movement sent from authoritative clients to the server

    [Tooltip("Threshold for new data to be sent to clients, recommended = 0.01f")]
    [Range(0.001f, 1.0f)]
    public float movement_threshold = 0.01f;
    [Tooltip("Threshold for rotations in degrees, recommended = 0.75f or higher")]
    [Range(0.5f, 360.0f)]
    public float rotation_threshold = 0.75f;

    //used for sender tracking
    Vector3 previous_position;
    Quaternion previous_rotation;

    //used for determining if stopped on the sender
    bool isMoving = false;

    /// <summary>
    /// Interpolation rate, default is 0.1f
    /// </summary>
    static readonly float interpolation_rate = 0.1f;
    /// <summary>
    /// Extrapolate duration, default is 0.1f
    /// </summary>
    static readonly float extrapolate_duration = 0.1f;

    /// <summary>
    /// Message SendRate, Send update every SnedRate seconds. Default is (0.05f, or every 50ms)
    /// </summary>
    [Range(0.01f, 100.0f)]
    [Tooltip("The message send rate for the transform syncing, recommended at 0.05f")]
    public float SendRate = 0.05f;

    void Start()
    {
        RegisterMessage((byte)UpdateTypes.Position, OnReceivePosition);
        RegisterMessage((byte)UpdateTypes.Rotation, OnReceiveRotation);
        RegisterMessage((byte)UpdateTypes.FinalPosition, OnReceiveFinal);

        if (IsServer || IsHost || HaveAuthority)
            StartCoroutine(SendRoutine());
    }

    /// <summary>
    /// Returns true if the input vectors are approximatly equal
    /// </summary>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <param name="epsilon"></param>
    /// <returns></returns>
    bool ApproxEqual(Vector3 v1, Vector3 v2, float epsilon)
    {
        return Vector3.Distance(v1, v2) < epsilon;
    }
    bool ApproxEqual(Quaternion q1, Quaternion q2, float epsilon)
    {
        return Quaternion.Angle(q1, q2) < epsilon;
    }

    /// <summary>
    /// Send a position to clients
    /// </summary>
    /// <param name="value"></param>
    void SendPosition(Vector3 value)
    {
        NetOutgoingMessage msg = GetNewMessage((byte)UpdateTypes.Position);
        msg.Write(value);
        Send(msg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Send position to server, authorative
    /// </summary>
    /// <param name="value"></param>
    void SendPositionAuthorative(Vector3 value)
    {
        NetOutgoingMessage msg = GetNewMessage((byte)UpdateTypes.Position);
        msg.Write(value);
        Send(msg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Host updates all positions but its own
    /// </summary>
    /// <param name="value"></param>
    void SendPositionExclusive(Vector3 value)
    {
        //to prevent loops, the host doesn't send to itself (it shares server objects so it doesn't need to anyways)
        NetOutgoingMessage omsg = GetNewMessage((byte)UpdateTypes.Position);
        omsg.Write(value);
        SendExclusive(omsg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Send a rotation to clients
    /// </summary>
    /// <param name="value"></param>
    void SendRotation(Quaternion value)
    {
        NetOutgoingMessage msg = GetNewMessage((byte)UpdateTypes.Rotation);
        msg.Write(value);
        Send(msg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Send a rotation to server, authorative
    /// </summary>
    /// <param name="value"></param>
    void SendRotationAuthorative(Quaternion value)
    {
        NetOutgoingMessage msg = GetNewMessage((byte)UpdateTypes.Rotation);
        msg.Write(value);
        Send(msg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Updates all clients on a host server but its own
    /// </summary>
    /// <param name="value"></param>
    void SendRotationExclusive(Quaternion value)
    {
        //to prevent loops, the host doesn't send to itself (it shares server objects so it doesn't need to anyways)
        NetOutgoingMessage omsg = GetNewMessage((byte)UpdateTypes.Rotation);
        omsg.Write(value);
        SendExclusive(omsg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Called to update position from server
    /// </summary>
    /// <param name="msg"></param>
    void OnReceivePosition(NetIncomingMessage msg)
    {
        //can still receive message, so just return if we have authority
        if (HaveAuthority) return;

        Position = msg.ReadVector3();
        UpdatedPosition = true;

        if (!PosInitialized)
        {
            PrevPosition = Position;
            PosInitialized = true;
        }

        //if this is a host or server, then we need to send the input information to all clients
        if (IsServer)
        {
            SendPosition(Position);
        }
        else if (IsHost)
        {
            SendPositionExclusive(Position);
        }
    }
    /// <summary>
    /// called to update rotation from server
    /// </summary>
    /// <param name="msg"></param>
    void OnReceiveRotation(NetIncomingMessage msg)
    {
        //can still receive message, so just return if we have authority
        if (HaveAuthority) return;

        Rotation = msg.ReadQuaternion();
        UpdatedRotation = true;

        if (!RotInitialized)
        {
            PrevRotation = Rotation;
            RotInitialized = true;
        }

        //if this is a host or server, then we need to send the input information to all clients
        if (IsServer)
        {
            SendRotation(Rotation);
        }
        else if (IsHost)
        {
            SendRotationExclusive(Rotation);
        }
    }
    /// <summary>
    /// Send final position update (stopped detecting movement)
    /// </summary>
    /// <param name="value"></param>
    void SendFinal(Vector3 value)
    {
        NetOutgoingMessage msg = GetNewMessage((byte)UpdateTypes.FinalPosition);
        msg.Write(value);
        Send(msg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Send final position update (stopped detecting movement)
    /// </summary>
    /// <param name="value"></param>
    void SendFinalAuthorative(Vector3 value)
    {
        NetOutgoingMessage msg = GetNewMessage((byte)UpdateTypes.FinalPosition);
        msg.Write(value);
        Send(msg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    void SendFinalExclusive(Vector3 value)
    {
        //to prevent loops, the host doesn't send to itself (it shares server objects so it doesn't need to anyways)
        NetOutgoingMessage omsg = GetNewMessage((byte)UpdateTypes.FinalPosition);
        omsg.Write(value);
        SendExclusive(omsg, NetDeliveryMethod.ReliableSequenced, (int)LidgrenNetManager.Channels.TransformUpdate);
    }
    /// <summary>
    /// Receive final position update, don't extrapolate
    /// </summary>
    /// <param name="msg"></param>
    void OnReceiveFinal(NetIncomingMessage msg)
    {
        //can still receive message, so just return if we have authority
        if (HaveAuthority) return;

        Position = msg.ReadVector3();
        PositionIsFinal = true;

        //if this is a host or server, then we need to send the input information to all clients
        if (IsServer)
        {
            SendFinal(Position);
        }
        else if (IsHost)
        {
            SendFinalExclusive(Position);
        }
    }

    //used for receiver, current value to interpolate towards
    Vector3 Position;
    Quaternion Rotation;

    //previously received information
    Vector3 PrevPosition;
    Quaternion PrevRotation;

    //booleans used for receiving side
    bool UpdatedPosition = false;
    bool UpdatedRotation = false;

    //cannot start synching until initialized
    bool PosInitialized = false;
    bool RotInitialized = false;

    //is the current position the final? If not, then extrapolate
    bool PositionIsFinal = false;

    /// <summary>
    /// Time started extrapolating
    /// </summary>
    float TimeExtrapolateStart = 0.0f;
    /// <summary>
    /// Time spent extrapolating
    /// </summary>
    float ExtrapolateTime
    {
        get
        {
            return Time.realtimeSinceStartup - TimeExtrapolateStart;
        }
    }
    /// <summary>
    /// Are we extrapolating?
    /// </summary>
    bool is_extrapolating = false;

    /// <summary>
    /// Sending data to remote objects
    /// </summary>
    /// <returns></returns>
    IEnumerator SendRoutine()
    {
        yield return 0;

        while (this != null)
        {
            if (!ApproxEqual(previous_position, transform.position, movement_threshold))
            {
                //we include the !PosInitialized because the server may be receiving input from an authoritative client,
                //if it is, then we update immediatly in the OnReceive Callbacks
                if (IsServer && HaveAuthority) SendPosition(transform.position);
                else if (IsHost && HaveAuthority) SendPositionExclusive(transform.position);
                else if (IsClient && HaveAuthority) SendPositionAuthorative(transform.position);

                previous_position = transform.position;
                if (!isMoving) isMoving = true;
            }
            else if (isMoving)
            {
                isMoving = false;
                //we include the !PosInitialized because the server may be receiving input from an authoritative client,
                //if it is, then we update immediatly in the OnReceive Callbacks
                if (IsServer && HaveAuthority) SendFinal(transform.position);
                else if (IsHost && HaveAuthority) SendPositionExclusive(transform.position);
                else if (IsClient && HaveAuthority) SendFinalAuthorative(transform.position);
            }

            //update if rotated
            if (!ApproxEqual(previous_rotation, transform.rotation, rotation_threshold))
            {
                if (IsServer && HaveAuthority) SendRotation(transform.rotation);
                else if (IsHost && HaveAuthority) SendRotationExclusive(transform.rotation);
                else if (IsClient && HaveAuthority) SendRotationAuthorative(transform.rotation);

                previous_rotation = transform.rotation;
            }

            yield return new WaitForSeconds(SendRate);
        }
    }

    /// <summary>
    /// Updating position based on remote input
    /// </summary>
    void FixedUpdate()
    {
        //if this object has authority, then it moves itself and can ignore received message updates
        if (HaveAuthority) return;

        //will not do interpolation until the 'PosInitialied' or 'RotInitialized' values are set

        //update variables
        if (UpdatedPosition)
        {
            PrevPosition = transform.position;
            UpdatedPosition = false;
            PositionIsFinal = false;
            is_extrapolating = false;
        }
        //update variables
        if (UpdatedRotation)
        {
            PrevRotation = transform.rotation;
            UpdatedRotation = false;
        }

        //if the transform positions are not equal
        if ((PosInitialized && !ApproxEqual(transform.position, Position, 0.001f)) || is_extrapolating)
        {
            float InterpPerUpdate = Time.fixedDeltaTime / interpolation_rate;

            Vector3 velocity = (Position - PrevPosition).normalized * (Position - PrevPosition).magnitude;// divided by 1

            float dist_to_point = (Position - transform.position).magnitude;
            float travel_dist = (velocity * InterpPerUpdate).magnitude;

            Vector3 travel_direction = velocity.normalized;
            Vector3 actual_direction = (Position - transform.position).normalized;

            bool wrong_dir = !ApproxEqual(travel_direction, actual_direction, 0.001f);

            if (travel_dist > dist_to_point || wrong_dir)
            {
                //if receive stop, or we aren't extrapolating and are in the wrong direction
                if (PositionIsFinal || (wrong_dir && !is_extrapolating))
                {
                    transform.position = Position;
                }
                //extrapolate!! We extrapolate for extrapolate_duration, if still not received signal, then go back to 'Position'
                else
                {
                    //if extrapolating for too long...
                    if (is_extrapolating && ExtrapolateTime > extrapolate_duration)
                    {
                        transform.position = Position;
                        is_extrapolating = false;
                        goto update_rotation;
                    }

                    //if not extrapolating
                    if (!is_extrapolating)
                    {
                        is_extrapolating = true;
                        TimeExtrapolateStart = Time.realtimeSinceStartup;
                    }
                    //move it move it move it
                    transform.position += velocity * InterpPerUpdate;
                }
            }
            else
            {
                transform.position += velocity * InterpPerUpdate;
            }
        }

        update_rotation:;

        //if rotations aren't equal
        if (RotInitialized && !ApproxEqual(transform.rotation, Rotation, 0.5f))
        {
            float InterpPerUpdate = Time.fixedDeltaTime / interpolation_rate;
            Quaternion midRotation = Quaternion.Slerp(PrevRotation, Rotation, InterpPerUpdate);

            Vector3 delta = AngleDifference(PrevRotation.eulerAngles, midRotation.eulerAngles);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Rotation,
                Mathf.Max(Mathf.Abs(delta.z), Mathf.Max(Mathf.Abs(delta.x), Mathf.Abs(delta.y))));

            //no rotation extrapolation :[
        }
    }

    /// <summary>
    /// returns the difference, in degrees between a and b, a and b are expected to be eulerangles whos angles are all from 0 to 360
    /// Eg. a + AngleDifference(a, b) = b
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    Vector3 AngleDifference(Vector3 a, Vector3 b)
    {
        float x = b.x - a.x;
        float y = b.y - a.y;
        float z = b.z - a.z;

        x = x > 180 ? 360 - x : x;
        y = y > 180 ? 360 - y : y;
        z = z > 180 ? 360 - z : z;

        return new Vector3(x, y, z);
    }
    /// <summary>
    /// Adds two eulerangles together
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    Vector3 AngleAdd(Vector3 a, Vector3 b)
    {
        Vector3 newAngle = new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
        newAngle.x = newAngle.x >= 360 ? 360 - newAngle.x : newAngle.x;
        newAngle.y = newAngle.y >= 360 ? 360 - newAngle.y : newAngle.y;
        newAngle.z = newAngle.z >= 360 ? 360 - newAngle.z : newAngle.z;
        return newAngle;
    }

    /// <summary>
    /// Types used for this class messages
    /// </summary>
    enum UpdateTypes : byte
    {
        Position = 0,
        Rotation = 1,
        FinalPosition = 2
    }
}
