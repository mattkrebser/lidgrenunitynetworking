﻿using UnityEngine;
using System.Collections;
using Lidgren.Network;

/// <summary>
/// This class is meant to be overriden so that the events can be collected. These events are called by the LidgrenNetManager
/// </summary>
public class LidgrenNetEvents : MonoBehaviour
{
    public LidgrenNetManager network
    {
        get
        {
            return LidgrenNetManager.Singleton;
        }
    }

    /// <summary>
    /// receive a message from remote network manager. This does not intercept messages from other game objects. This only responds to
    /// the 'Send' functions on the LidgrenNetManager
    /// </summary>
    /// <param name="msg"></param>
    public virtual void OnReceiveMessage(NetIncomingMessage msg)
    {

    }

    /// <summary>
    /// Called on the server when a new connection is made. This function is called before the player object is created.
    /// It can be used for initializtion. The player objects will not be spawned on the server or client until this function returns.
    /// This function is not called for the host client.
    /// </summary>
    /// <param name="connection"></param>
    /// <returns></returns>
    public virtual IEnumerator OnInitializeClient(NetConnection connection)
    {
        yield break;
    }
    /// <summary>
    /// Called on the server when a new client connects. Called right after the server spawns the player object. Not called on host client connect.
    /// </summary>
    public virtual void OnClientConnect(NetConnection connection)
    {

    }
    /// <summary>
    /// Called on the server when a client disconnects. Called right before the player object and other client authority objects are destroyed.
    /// Not called on host client disconnect.
    /// </summary>
    /// <param name="connection"></param>
    public virtual void OnClientDisconnect(NetConnection connection)
    {

    }
    /// <summary>
    /// Called on the client when it connects to the server. Called after the online scene is loaded. Not called for host client.
    /// </summary>
    public virtual void OnConnect()
    {

    }
    /// <summary>
    /// Called on the client when it disconnects from the server. Called right before the online scene is unloaded.
    /// Not called if 'LeaveNetwork' is used on the client. Not called for host client disconnect.
    /// </summary>
    public virtual void OnDisconnect()
    {

    }
    /// <summary>
    /// Called on all network types immediatly before the offline scene is loaded
    /// </summary>
    public virtual void OnLoadOfflineScene()
    {

    }
    /// <summary>
    /// Called on all network types immediatly before the online scene is loaded
    /// </summary>
    public virtual void OnLoadOnlineScene()
    {

    }
}
