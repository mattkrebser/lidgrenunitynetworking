﻿[System.Serializable]
public class LevelIndex
{    
    /// <summary>
    /// Scene build index
    /// </summary>
    public int value = -1;
}