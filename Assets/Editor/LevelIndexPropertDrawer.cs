﻿using UnityEngine;
using UnityEditor;
//This file must be in Assets\Editor\

[CustomPropertyDrawer(typeof(LevelIndex))]
public class LevelIndexPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string[] levelNames = new string[EditorBuildSettings.scenes.Length];
        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
        {
            if (!EditorBuildSettings.scenes[i].enabled) continue;

            string scenePath = EditorBuildSettings.scenes[i].path;
            string[] seperators = new string[2] { "/", "." };
            string[] splitPath = scenePath.Split(seperators, System.StringSplitOptions.None);
            levelNames[i] = splitPath[splitPath.Length - 2];
        }

        EditorGUI.BeginProperty(position, label, property);

        Rect rect = new Rect(position.x, position.y , position.width, position.height);


        SerializedProperty levelIndexProp = property.FindPropertyRelative("value");
        int selectedLevelIndex = levelIndexProp.intValue;
        selectedLevelIndex = EditorGUI.Popup(rect, property.name, selectedLevelIndex, levelNames);
        levelIndexProp.intValue = selectedLevelIndex;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) ;
    }
}