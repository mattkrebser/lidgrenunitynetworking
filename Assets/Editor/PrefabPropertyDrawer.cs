﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(LidgrenNetManager))]
public class PrefabPropertyDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("DEBUG"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("use_port"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("connect_to_ip"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("ApplicationName"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("MaxConnections"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("TimeOut"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("simulated_latency"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("simulated_packet_loss"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("PlayerObject"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("SpawnablePrefabs"), true);
        serializedObject.ApplyModifiedProperties();

        LidgrenNetManager o = GameObject.FindObjectOfType(typeof(LidgrenNetManager)) as LidgrenNetManager;
        if (o != null)
        {
            List<GameObject> objs = new List<GameObject>(o.AllSpawnablePrefabs);

            for (int i = objs.Count - 1; i >= 0; i--)
            {
                GameObject g = objs[i];

                if (g == null)
                {
                    Debug.Log("Null prefab on the LidgrenNetManager prefab list"); continue;
                }

                LidgrenNetBehaviour nb = g.GetComponent(typeof(LidgrenNetBehaviour)) as LidgrenNetBehaviour;

                if (nb == null)
                {
                    Debug.Log("Error, the game object " + g.name + " does not have a LidgrenNetBehaviour component!"); continue;
                }

                nb.PrefabID = i;

                PrefabUtility.RecordPrefabInstancePropertyModifications(g);

                if (PrefabUtility.GetPrefabParent(g) != null)
                    Debug.Log("NetManager object is a scene object, please only use prefabs from the file system");
            }
        }
    }
}