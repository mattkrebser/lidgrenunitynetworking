# README #

Re-Implemented core Unity3D Multiplayer networking features using Lidgren Network library.
Lidgren-network:
https://github.com/lidgren/lidgren-network-gen3

### What is this repository for? ###

* A simple Networking system for Unity Games
* Very similar to UNet NetworkManager

### Features ###

* Game Object synchronization using the provided Network Spawn and Destroy functions.
* Client, host, server networks all supported.
* Interpolating/extrapolating Network transform. 
* Latency and packet drop simulation
* Automatically drop slow connections
* Syncvars, with hook functions

### How do I get set up? ###

* This repository is a Unity project folder, download the folder and open it in Unity
* The Lidgren Network Manager is very similar to the Unity Network Manager, so Unity documentation can be helpful
* The workflow is also similar, but this System only uses object messages for communication between clients.

** Some of the Differences**

1.   Only has Message system for inter-object communication, see the LidgrenNetTransform component for examples. Also, look at the LidgrenNetBehaviour object. It has many comments (as do all of the components)

2. Note, with the messages- There is only allowed to be 256 unique callback functions per object Instance. Message types are unique for every unity component. Messages will be automatically received on the same object they are sent from.

3. Read the descriptions for all of the public fields/properties/methods for a complete understanding of the differences between UNet.

4. On Object Authority, Either the server/host has authority over an object, or a remote client does.

5. Syncvars must be given a (ushort) type and registered to synchronize between clients

** Notes **

* This library is fairly new, I am still testing it